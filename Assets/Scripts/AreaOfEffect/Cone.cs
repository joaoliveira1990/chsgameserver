﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public class Cone : MonoBehaviour
{
    private float areaRadius;
    private float damage;
    private float destroyAfter;
    private float damageTickSpeed;
    private EntityType eligibleTargets;

    public void Initialize(float _areaRadius, float _damage, float _destroyAfter, float _damageTickSpeed, EntityType _eligibleTargets)
    {
        areaRadius = _areaRadius;
        damage = _damage;
        destroyAfter = _destroyAfter;
        damageTickSpeed = _damageTickSpeed;
        eligibleTargets = _eligibleTargets;

        StartCoroutine(DestroyAfter());

        if (this.damageTickSpeed > 0f)
        {
            StartCoroutine(ApplyDamage());
        }
        else
        {
            ApplyDamage();
        }
    }

    private IEnumerator DestroyAfter()
    {
        yield return new WaitForSeconds(this.destroyAfter);
        StopCoroutine("DestroyAfter");
        StopCoroutine("ApplyDamage");
        Destroy(this.gameObject);
    }

    private IEnumerator ApplyDamage()
    {
        yield return new WaitForSeconds(this.damageTickSpeed);

        DealDamage();

        StartCoroutine(ApplyDamage());
    }

    private void DealDamage()
    {
        Collider[] hitColliders = Physics.OverlapBox(gameObject.transform.position, transform.localScale, Quaternion.identity);

        foreach (Collider collider in hitColliders)
        {
            GameObject target = collider.gameObject;

            if (eligibleTargets == EntityType.player)
            {
                if (target.TryGetComponent(out Player targetPlayer))
                {
                    targetPlayer.TakeDamage(this.damage);
                }
            }
            else if (eligibleTargets == EntityType.enemy)
            {
                if (target.TryGetComponent(out Enemy e))
                {
                    e.TakeDamage(this.damage, target.GetComponent<Enemy>().id, 0.8f);
                }
            }
        }
    }

    /*
        private void DealDamage()
        {

            RaycastHit[] hits = Physics.SphereCastAll(transform.position, areaRadius, new Vector3(0f, -1f, 0f));

            foreach (RaycastHit hit in hits)
            {
                GameObject target = hit.collider.gameObject;

                if (eligibleTargets == EntityType.player)
                {
                    if (target.TryGetComponent(out Player targetPlayer))
                    {
                        targetPlayer.TakeDamage(this.damage);
                    }
                }
                else if (eligibleTargets == EntityType.enemy)
                {
                    if (target.TryGetComponent(out Enemy e))
                    {
                        e.TakeDamage(this.damage, target.GetComponent<Enemy>().id, 0.8f);
                    }
                }
            }
        }
        */
}
