using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CombatStats : MonoBehaviour
{
    public int strength = 10;
    public int intellect = 10;
    public int agility = 10;
    public int stamina = 10;
    public int armor = 10;
    public int magicResist = 0;
    public int healthRegenValue = 10;
    public int manaRegenValue = 10;
    public float attackSpeed = 0f;
    public float damage = 1f;
    public float spellDamage = 1f;
    public float maxMana = 100;
    public float mana = 100f;
    public float rage = 100f;
    public int energy = 100;
    public float movementSpeed = 1f;
    public float critChance = 0f;
    public float manaRegenTimeInterval = 5f;
    private bool isManaRegenRunning = false;
    public float healingTaken = 0f;
    public float healPercentage = 0f;
    public bool hasDeathImmunity = false;
    private bool isSilenced = false;
    private bool isStunned = false;
    private bool isSnared = false;
    private bool isSlowed = false;
    public Constants.ResourceTypes resourceType;
    private List<Effect> effects = new List<Effect>();
    // List<Effect> debuffs = new List<Effect>();
    private List<Effect> passives = new List<Effect>();
    public void Initialize()
    {
        if (this.gameObject.tag == "Player")
        {
            Player p = this.GetComponent<Player>();
            DataResources.SetPlayerStats(p);
            ServerSend.UpdateStatWindow(p.id, strength, intellect, agility, stamina, armor, magicResist, healthRegenValue, manaRegenValue, critChance, attackSpeed, damage, spellDamage);
        }
    }

    // * Effects Handlers

    /// <summary>Create effect or add stack increment if already exists. Capped to max stacks and refreshes lifetime</summary>
    /// <returns>New/Existing Effect instance with updated values</returns>
    public Effect AddEffect(Effect _ePrefab)
    {
        CleanEffects();

        Effect existingEffect = GetEffectByName(_ePrefab.name);
        if (existingEffect == null)
        {
            Effect newEffect = Instantiate(_ePrefab, this.gameObject.transform);
            newEffect.Initialize(this.gameObject);
            newEffect.stacks = 1; // TODO: set default to 1
            this.effects.Add(newEffect);

            existingEffect = newEffect;
        }
        else
        {
            if (existingEffect.stacks < existingEffect.maxStacks) existingEffect.stacks += 1;
            // TODO: reset timer
        }

        // ! Do same for enemy
        if (this.gameObject.tag == "Player")
        {
            Player player = this.GetComponent<Player>();
            ServerSend.PlayerAddEffect(player.id, existingEffect.name, existingEffect.lifeTime, existingEffect.stacks, "player");
        }

        return existingEffect;
    }

    public Effect GetEffectByName(Constants.AllEffects _effectName)
    {
        return this.effects.Find(effect => effect.name == _effectName);
    }
    public bool HasEffect(Constants.AllEffects _effectName)
    {
        return this.effects.Find(effect => effect.name == _effectName) != null;
    }

    // ! Stop using this
    public List<Effect> GetEffectsByName(Constants.AllEffects _effectName)
    {
        List<Effect> result = new List<Effect>();
        foreach (Effect e in effects)
        {
            if (e == null) continue;
            if (e.name == _effectName)
            {
                result.Add(e);
            }
        }
        return result;
    }

    public int GetNumberOfStacksFromEffect(Constants.AllEffects _effectName)
    {
        Effect foundEffect = this.effects.Find(effect => effect.name == _effectName);

        if (foundEffect == null) return 0;
        else return foundEffect.stacks;
    }

    public void RemoveEffectByName(Constants.AllEffects _effectName)
    {
        CleanEffects();
        this.effects.RemoveAll(e => e.name == _effectName);

        Player player = this.GetComponent<Player>();
        ServerSend.PlayerRemoveEffect(player.id, _effectName);
    }

    /// <summary>To make sure all expired effects are removed</summary>
    public void CleanEffects()
    {
        this.effects.RemoveAll(effect => effect.isDestroyed);
    }

    // * Damage & Resource

    public float GetPlayerDamage()
    {
        if (UnityEngine.Random.value <= critChance)
        {
            return damage * 2;
        }
        else return damage;
    }

    public void ResetRage()
    {
        rage = 0;
    }
    public void AddRage(int _quantity)
    {
        this.rage += _quantity;
        if (this.rage > 100) this.rage = 100;
        else if (this.rage < 0) this.rage = 0;

        ServerSend.PlayerRage(this.GetComponent<Player>());
    }

    public void AddMana(int _quantity)
    {
        this.mana += _quantity;
        if (this.mana > this.maxMana)
        {
            this.mana = this.maxMana;
            this.isManaRegenRunning = false;
            StopCoroutine("RegenMana");
        }
        else if (this.mana < 0)
        {
            this.mana = 0;
            this.isManaRegenRunning = false;
            StopCoroutine("RegenMana");
        }
        else if (!this.isManaRegenRunning)
        {
            StartCoroutine(RegenMana(this.manaRegenTimeInterval));
            this.isManaRegenRunning = true;
        }
        ServerSend.PlayerMana(this.GetComponent<Player>());
    }

    private IEnumerator RegenMana(float _manaRegenTimeInterval)
    {
        yield return new WaitForSeconds(_manaRegenTimeInterval);
        isManaRegenRunning = false;
        AddMana(manaRegenValue);
    }

    public void SetCC(Constants.CC ccType, bool isCCed, float _movementSpeedModifier)
    {
        switch (ccType)
        {
            case Constants.CC.silence:
                isSilenced = isCCed;
                break;
            case Constants.CC.stun:
                isStunned = isCCed;
                break;
            case Constants.CC.snare:
                isSnared = isCCed;
                break;
            case Constants.CC.slow:
                isSlowed = isCCed;
                SetPlayerSpeed(_movementSpeedModifier);
                break;
            case Constants.CC.none:
                isSilenced = false;
                isStunned = false;
                isSnared = false;
                isSlowed = false;
                break;
        }
    }
    public bool[] GetCC()
    {
        return new bool[] { isSilenced, isStunned, isSnared, isSlowed };
    }
    private void SetPlayerSpeed(float _movementSpeed)
    {
        float baseMovementSpeed = this.GetComponent<Player>().baseMovementSpeed;
        NavMeshAgent agent = this.GetComponent<Player>().GetComponent<NavMeshAgent>();
        float newSpeed = baseMovementSpeed * (_movementSpeed / 100);

        agent.speed += newSpeed;

        ServerSend.PlayerSetSpeed(this.GetComponent<Player>().id, agent.speed);
    }
    public void AddItemStats(int _itemId)
    {
        UIItemInfo item = UIItemDatabase.Instance.GetByID(_itemId);

        Debug.Log("ID getbyid" + item.ID);
        Debug.Log("NAME getbyid" + item.Name);

        this.damage += item.AttackDamage;
        this.spellDamage += item.SpellDamage;
        this.attackSpeed += item.Haste;
        this.magicResist += item.MagicResist;
        this.armor += item.Armor;
        this.stamina += item.Stamina;
        this.strength += item.Strength;
        this.agility += item.Agility;
        this.intellect += item.Intellect;
        this.maxMana += item.Mana;
        this.mana += item.Mana;
        this.critChance += item.Crit;

        ServerSend.UpdateStatWindow(this.GetComponent<Player>().id, strength, intellect, agility, stamina, armor, magicResist, healthRegenValue, manaRegenValue, critChance, attackSpeed, damage, spellDamage);
    }
}
