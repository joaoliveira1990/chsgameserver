﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CombatStatsEnemy : MonoBehaviour
{
    // TODO: Mini copy of CombatStats for Player. 
    // ? One solution: have Player and Enemy in the same class family and use polymorphism.
    // ? Another solution: have class family of CombatStatsPlayer and CombatStatsEnemy

    public float movementSpeed = 1f;
    private bool isSilenced = false;
    private bool isStunned = false;
    private bool isSnared = false;
    private bool isSlowed = false;
    public List<Effect> effects = new List<Effect>();

    [SerializeField] private float hitboxRadius = 0f;

    public float GetHitboxRadius()
    {
        return hitboxRadius;
    }
    public void AddEffect(Effect _e)
    {
        CleanEffects();
        
        Effect existingEffect = GetEffectByName(_e.name);

        if (existingEffect != null)
        {    
            if (existingEffect.stacks < existingEffect.maxStacks) 
            {
                existingEffect.stacks += 1;
            }
            else
            {
                existingEffect.RestartCoroutine();
                ServerSend.PlayerAddEffect(this.gameObject.GetComponent<Enemy>().id, existingEffect.name, existingEffect.lifeTime, existingEffect.stacks, "enemy");
            }
            return;
        }

        Effect effect = Instantiate(_e, this.gameObject.transform);
        effect.Initialize(this.gameObject);
        this.effects.Add(effect);
        int effectId = (int)effect.name;
        ServerSend.PlayerAddEffect(this.gameObject.GetComponent<Enemy>().id, effect.name, effect.lifeTime, effect.stacks, "enemy");
/*
        Effect existingEffect = GetEffectByName(_e.name);
        if (existingEffect == null)
        {
            Effect effect = Instantiate(_e, this.gameObject.transform);
            effect.Initialize(this.gameObject);
            this.effects.Add(effect);
            int effectId = (int)effect.name;
            ServerSend.PlayerAddEffect(this.gameObject.GetComponent<Enemy>().id, effect.name, effect.lifeTime, effect.stacks, "enemy");
        }
        else
        {
            if (existingEffect.stacks < existingEffect.maxStacks) existingEffect.stacks += 1;
            else
            {
                existingEffect.isDestroyed = true;
                CleanEffects();
            }
        }
*/
    }

    public Effect GetEffectByName(Constants.AllEffects _effectName)
    {
        Effect foundEffect = this.effects.Find(effect => effect.name == _effectName);
        return foundEffect;
    }
    public bool HasEffect(Constants.AllEffects _effectName)
    {
        return this.effects.Find(effect => effect.name == _effectName) != null;
    }

    public int GetNumberOfStacks(Constants.AllEffects _effectName)
    {
        Effect foundEffect = this.effects.Find(effect => effect.name == _effectName);
        if (foundEffect == null)
        {
            return 0;
        }
        return foundEffect.stacks;
    }

    // ! Stop using this
    public List<Effect> GetEffectsByName(Constants.AllEffects _effectName)
    {
        List<Effect> result = new List<Effect>();
        foreach (Effect e in this.effects)
        {
            if (e == null) continue;
            if (e.name == _effectName)
            {
                result.Add(e);
            }
        }
        return result;
    }

    public void RemoveEffectsByName(Constants.AllEffects _effectName)
    {
        CleanEffects();
        effects.RemoveAll(e => e.name == _effectName);
    }

    public void CleanEffects()
    {
        effects.RemoveAll(e => e.isDestroyed);
    }

    public void SetCC(Constants.CC ccType, bool isCCed, float _movementSpeedModifier)
    {
        switch (ccType)
        {
            case Constants.CC.silence:
                isSilenced = isCCed;
                break;
            case Constants.CC.stun:
                isStunned = isCCed;
                break;
            case Constants.CC.snare:
                isSnared = isCCed;
                break;
            case Constants.CC.slow:
                isSlowed = isCCed;
                SetEnemySpeed(_movementSpeedModifier);
                break;
            case Constants.CC.none:
                isSilenced = false;
                isStunned = false;
                isSnared = false;
                isSlowed = false;
                break;
        }
    }
    public bool[] GetCC()
    {
        return new bool[] { isSilenced, isStunned, isSnared, isSlowed };
    }
    private void SetEnemySpeed(float _movementSpeed)
    {
        float baseMovementSpeed = this.GetComponent<Enemy>().baseMovementSpeed;
        NavMeshAgent agent = this.GetComponent<Enemy>().GetComponent<NavMeshAgent>();
        float newSpeed = baseMovementSpeed * (_movementSpeed / 100);

        agent.speed += newSpeed;

        ServerSend.EnemySetSpeed(this.GetComponent<Enemy>().id, agent.speed);
    }
}
