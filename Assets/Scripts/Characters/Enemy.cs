﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class Enemy : MonoBehaviour
{
    public static int maxEnemies = 10;
    public static Dictionary<int, Enemy> enemies = new Dictionary<int, Enemy>();
    public static int nextEnemyId = 1;
    public int id;
    public EnemyState state;
    public Player target;
    public CharacterController controller;
    public Transform shootOrigin;
    public Transform breathOrigin;
    public float gravity = -9.81f;
    public float patrolSpeed = 2f;
    public float chaseSpeed = 8f;
    public float health;
    public float maxHealth = 100f;
    public float detectionRange = 10f;
    public float shootRange = 2f;
    public float patrolDuration = 6f;
    public float attackSpeed;
    public float idleDuration = 3f;
    public float damage = 6f;
    public float baseMovementSpeed = 0f;
    private bool isPatrolRoutineRunning;
    private bool isAttackRoutineRunning;
    public bool isCastChase = false;
    public float castChaseRange = -1.0f;
    public bool isCasting = false;
    public GameObject basicAttack;
    public List<GameObject> spellPrefabs = new List<GameObject>();
    private List<GameObject> spells = new List<GameObject>();
    GameObject basicAttackInstance = null;
    public bool isTargetDummy = false;
    NavMeshAgent agent = null;
    private NavMeshPath path = null;
    public Vector3[] waypoints;
    public Constants.EnemyTypes enemyType;
    private Dictionary<int, float> aggroTable = new Dictionary<int, float>();
    private int randomSpellIndex = 0;
    public bool IsDead = false;
    public Vector3 spawnPosition = Vector3.zero;
    private bool canMoveToSector = true;
    public int goldValue = 30;
    public int xpValue = 100;

    private void Start()
    {
        //GameObject rangedAttack = Resources.Load("Spells/RangedAttack") as GameObject;
        basicAttackInstance = Instantiate(basicAttack, this.gameObject.transform);
        InitializeEnemySpells();

        id = nextEnemyId;
        nextEnemyId++;
        enemies.Add(id, this);

        agent = this.GetComponent<NavMeshAgent>();
        path = new NavMeshPath();

        baseMovementSpeed = agent.speed;

        ServerSend.SpawnEnemy(this);

        UpdateState(EnemyState.idle);
        health = maxHealth;

        this.spawnPosition = this.transform.transform.position;

        this.shootRange += this.GetComponent<CharacterController>().radius;

        //????
        gravity *= Time.fixedDeltaTime * Time.fixedDeltaTime;
        patrolSpeed *= Time.fixedDeltaTime;
        chaseSpeed *= Time.fixedDeltaTime;
    }

    private void InitializeEnemySpells()
    {
        foreach (GameObject spell in spellPrefabs)
        {
            GameObject go = Instantiate(spell, this.gameObject.transform);
            go.SetActive(true);
            spells.Add(go);
        }
    }

    void OnDrawGizmos()
    {
        if (target != null)
        {
            Gizmos.DrawLine(transform.position, target.transform.position);
        }
    }

    private void FixedUpdate()
    {
        if (this.IsDead) return;
        switch (state)
        {
            case EnemyState.idle:
                RunIdle();
                break;
            case EnemyState.patrol:
                RunPatrol();
                break;
            case EnemyState.chase:
                RunChase();
                break;
            case EnemyState.attack:
                RunAttack();
                break;
            case EnemyState.cast:
                RunCast();
                break;
            default:
                break;
        }
    }

    private void RunIdle()
    {
        if (IsPlayerDetected())
        {
            UpdateState(EnemyState.chase);
        }

        // TODO: Start patrol somehow
    }

    private void RunPatrol()
    {
        // - Detect enemy -> Chase
        if (IsPlayerDetected())
        {
            StopPath();
            isPatrolRoutineRunning = false;
            StopCoroutine(StartPatrol());

            UpdateState(EnemyState.chase);
            return;
        }

        // - Start Patrol
        if (!isPatrolRoutineRunning)
        {
            StartCoroutine(StartPatrol());
        }

        // - Continue Patrol
        MoveAndSendPath(this.target.transform.position);
    }

    private void RunChase()
    {
        CombatStatsEnemy enemyStats = this.GetComponent<CombatStatsEnemy>();
        if (enemyStats.GetCC()[1] || enemyStats.GetCC()[2]) return; // 0 - silence, 1 - stun, 2 - snare, 3 - slow
        Vector3 _enemyToPlayer = target.transform.position - transform.position;
        canMoveToSector = true;

        // - In Range -> Attack
        if (!this.isCastChase && _enemyToPlayer.magnitude <= shootRange)
        {
            StopPath();
            UpdateState(EnemyState.attack);
            return;
        }

        // - In Range -> Cast
        if (this.isCastChase && _enemyToPlayer.magnitude <= this.castChaseRange)
        {

            StopPath();
            UpdateState(EnemyState.cast);
            return;
        }

        // - Continue Chase
        MoveAndSendPath(this.target.transform.position);
    }

    private void RunAttack()
    {
        CombatStatsEnemy enemyStats = this.GetComponent<CombatStatsEnemy>();
        if (enemyStats.GetCC()[1]) return; // 0 - silence, 1 - stun, 2 - snare, 3 - slow
        if (target.GetComponent<Player>().IsDead())
        {
            target = null;
            StopAllCoroutines();
            MoveAndSendPath(spawnPosition);
            UpdateState(EnemyState.idle);
            return;
        }
        Vector3 _enemyToPlayer = target.transform.position - transform.position;
        transform.forward = new Vector3(_enemyToPlayer.x, 0f, _enemyToPlayer.z);

        // - Continue Attack
        if (_enemyToPlayer.magnitude <= shootRange)
        {
            if (canMoveToSector) //run once, once it attacks
            {
                MoveAndSendPath(target.GetComponent<Player>().GetNextSector());
            }

            canMoveToSector = false;

            RotateEnemy(_enemyToPlayer);

            if (!isAttackRoutineRunning)
            {
                StartCoroutine(StartAttack());
            }
            return;
        }

        // - Give up
        if (false)
        {
            // return;
        }

        // - Target out of range -> Chase
        UpdateState(EnemyState.chase);
    }

    private void RunCast()
    {
        CombatStatsEnemy enemyStats = this.GetComponent<CombatStatsEnemy>();
        if (enemyStats.GetCC()[0] || enemyStats.GetCC()[1]) return; // 0 - silence, 1 - stun, 2 - snare, 3 - slow
        // - Cast Once
        if (!this.isCasting) //TODO: Add cooldown!
        {
            Spell castingSpell = this.spells[randomSpellIndex].GetComponent<Spell>();

            if (!castingSpell.Cast(this.target.gameObject, this.gameObject))
            {
                // - Finish Cast -> Chase
                FinishCast(true);
                return;
            }
            float castTime = spells[randomSpellIndex].GetComponent<Spell>().castTime;
            if (castTime > 0)
            {
                this.isCasting = true;
                ServerSend.EnemyCastStarted(id, castTime + 0.2f, castingSpell.castAnimation); //casttime + 200 ms compensation
                StartCoroutine(Casting(castTime + 0.2f)); //casttime + 200 ms compensation 
            }
            else
            {
                this.isCastChase = false;
                UpdateState(EnemyState.chase);
            }
            return;
        }

        // - Out of Range -> Chase (isCastChase)
        // TODO: Verify out of range, go to chase. In chase it should detect in range again and change back to cast and run "Cast Once"

        // - Finish Cast -> Idle
        //FinishCast();
    }

    private IEnumerator Casting(float _castTime)
    {
        yield return new WaitForSeconds(_castTime);
        // TODO: PlayerCast
        FinishCast();
        isCasting = false;
        UpdateState(EnemyState.chase);
    }

    public void InterruptSpells()
    {
        StopCoroutine("Casting");
        isCasting = false;
        foreach (GameObject go in spells)
        {
            Spell s = go.GetComponent<Spell>();

            if (s.isCasting)
            {
                s.InterruptSpell();
            }
        }
        FinishCast(true);
    }

    private void FinishCast(bool forceFinish = false)
    {
        if (isCasting || forceFinish)
        {
            this.isCastChase = false;
            this.castChaseRange = 0f;
            UpdateState(EnemyState.chase);
            StopPath();
        }
    }

    private bool IsPlayerDetected()
    {
        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null && !_client.player.IsDead())
            {
                Vector3 _enemyToPlayer = _client.player.transform.position - transform.position;
                /*
                if (_enemyToPlayer.magnitude <= detectionRange)
                {
                    target = _client.player;
                    return true;
                }
                */
                if (IsInRange(_enemyToPlayer.magnitude))
                {
                    target = _client.player;
                    SetAggroTable(_client.player.id, 100f);

                    if (CanSeeTarget())
                    {
                        if (spells.Count > 0) StartCoroutine(TriggerAbility(2f));
                        return true;
                    }
                    else return false;
                }
            }
        }
        return false;
    }

    private IEnumerator TriggerAbility(float _castInSeconds)
    {
        yield return new WaitForSeconds(_castInSeconds);

        randomSpellIndex = UnityEngine.Random.Range(0, spells.Count);

        ServerLogging.instance.Debugger($"randomSpellIndex: {randomSpellIndex}");
        ServerLogging.instance.Debugger($"SPELLS COUNT: {spells.Count}");

        this.isCastChase = true;
        this.castChaseRange = spells[randomSpellIndex].GetComponent<Spell>().range;
        UpdateState(EnemyState.chase);

        StartCoroutine(TriggerAbility(14f));
    }
    private bool IsInRange(float _distanceToTarget)
    {
        return _distanceToTarget <= detectionRange;
    }

    public void StopPath()
    {
        agent.isStopped = true;
        waypoints = Array.Empty<Vector3>();
        ServerSend.EnemyPath(this, true);
    }

    public void RotateEnemy(Vector3 _enemyToPlayer)
    {
        Quaternion rotation = Quaternion.LookRotation(_enemyToPlayer);
        rotation.x = 0;
        this.transform.rotation = rotation;
        ServerSend.EnemyRotation(this);
    }

    private IEnumerator StartPatrol()
    {
        //isPatrolRoutineRunning = true;
        //Vector2 _randomPatrolDirection = Random.insideUnitSphere.normalized;
        //transform.forward = new Vector3(_randomPatrolDirection.x, 0f, _randomPatrolDirection.y);

        //yield return new WaitForSeconds(patrolDuration);

        //UpdateState(EnemyState.idle);

        yield return new WaitForSeconds(idleDuration);

        //UpdateState(EnemyState.patrol);
        //isPatrolRoutineRunning = false;
    }

    private IEnumerator StartAttack()
    {
        isAttackRoutineRunning = true;
        ServerSend.EnemyAttacked(this);
        basicAttackInstance.GetComponent<Spell>().Cast(this.target.gameObject, this.gameObject);

        yield return new WaitForSeconds(attackSpeed);
        isAttackRoutineRunning = false;
    }

    private void MoveAndSendPath(Vector3 _newPosition)
    {
        agent.isStopped = false;

        _newPosition.y = 1f;
        NavMesh.CalculatePath(transform.position, _newPosition, NavMesh.AllAreas, path);
        agent.SetPath(path);

        Quaternion rotationZeroX = Quaternion.LookRotation(path.corners[1] - this.transform.position);
        rotationZeroX.x = 0;
        this.transform.rotation = rotationZeroX;

        waypoints = path.corners;

        ServerSend.EnemyPath(this, false);
    }

    private void UpdateState(EnemyState _state)
    {
        state = _state;
        ServerSend.EnemyState(this);
    }

    public void TakeDamage(float _damage, int _playerId, float _threatModifier)
    {
        health -= _damage;
        if (health <= 0f && !IsDead)
        {
            SetAggroTable(_playerId, _damage * _threatModifier);//write to threat table in case enemy gets one shotted
            health = 0f;
            StopPath();
            enemies.Remove(id);
            this.IsDead = true;
            Destroy(gameObject, 2f);

            GiveRewardsToPlayers();
        }
        else
        {
            SetAggroTable(_playerId, _damage * _threatModifier);
            SetTargetWithMostThreat();
        }

        ServerSend.EnemyHealth(this);
    }

    private void GiveRewardsToPlayers()
    {
        foreach (KeyValuePair<int, float> kvp in aggroTable)
        {
            ServerLogging.instance.Debugger($"rewarding player with id {kvp.Key}");
            Server.clients[kvp.Key].player.SetRewards(goldValue, xpValue);
        }

    }

    public void Suicide()
    {
        health = 0f;
        StopPath();
        this.IsDead = true;
        Destroy(gameObject);

        ServerSend.EnemyHealth(this);
    }

    public void SetAggroTable(int _playerId, float _threat)
    {
        if (aggroTable.ContainsKey(_playerId))
        {
            aggroTable[_playerId] += _threat;
        }
        else
        {
            aggroTable.Add(_playerId, _threat);
        }
    }

    public void SetTargetWithMostThreat()
    {
        if (aggroTable.Count > 0)
        {

            foreach (KeyValuePair<int, float> kvp in aggroTable)
            {
                ServerLogging.instance.Debugger($"Key = {kvp.Key}, Value = {kvp.Value}");
            }

            List<KeyValuePair<int, float>> orderedAggroTable = aggroTable.OrderBy(d => -d.Value).ToList();
            int playerIdWithMostThreat = orderedAggroTable[0].Key;
            if (target != null)
            {
                if (playerIdWithMostThreat != target.id)
                {
                    target = Server.clients[playerIdWithMostThreat].player;
                    UpdateState(EnemyState.chase);
                }
            }
            else
            {
                target = Server.clients[playerIdWithMostThreat].player;
                if (spells.Count > 0) StartCoroutine(TriggerAbility(2f));
                UpdateState(EnemyState.chase);
            }

        }
    }

    public Dictionary<int, float> GetAggroTable()
    {
        return this.aggroTable;
    }
    public float GetThreadByPlayerId(int _id)
    {
        return aggroTable[_id];
    }
    public bool CanSeeTarget()
    {

        if (target == null)
        {
            return false;
        }

        DrawPrimitives.instance.DrawRay(shootOrigin.position, target.transform.position - transform.position);

        if (Physics.Raycast(shootOrigin.position, target.transform.position - transform.position, out RaycastHit _hit))
        {
            if (_hit.collider.CompareTag("Player"))
            {
                return true;
            }
        }
        return false;
    }
}

public enum EnemyState
{
    idle,
    patrol,
    chase,
    attack,
    cast
}
