﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float frequency = 3f;

    private void Start()
    {
        //StartCoroutine(SpawnEnemy());
    }

    private IEnumerator SpawnEnemyTimer()
    {
        yield return new WaitForSeconds(frequency);

        if (Enemy.enemies.Count < Enemy.maxEnemies)
        {
            NetworkManager.instance.InstantiateEnemy(transform.position, (int)Constants.EnemyTypes.rangedNormal);
        }

        StartCoroutine(SpawnEnemyTimer());
    }

    public GameObject SpawnEnemy(Constants.EnemyTypes _type)
    {
        return NetworkManager.instance.InstantiateEnemy(transform.position, (int)_type);
    }
}
