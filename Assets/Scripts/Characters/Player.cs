﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    #region Properties

    // - Attributes
    public int id;
    public string username;
    public int selectedChampion;
    public int selectedClass = 0;
    public int selectedSpec = 0;
    public int team;
    //public CharacterController controller;
    public Transform shootOrigin;
    public float gravity = -9.18f;
    private float moveSpeed = 5f;
    public float jumpSpeed = 5f;
    public float throwForce = 600f;
    private float attackSpeed = 2f;
    public float maxHealth;
    public float shootRange = 4f;
    public float baseMovementSpeed = 0f;

    // - Status/Flags
    public float health;
    // public int itemAmount = 0;
    // public int maxItemAmount = 3;
    public PlayerState state;
    public bool isAttackRoutineRunning;
    public bool isCastChase = false;
    public float castChaseRange = -1.0f;
    public bool isCasting = false;
    private int castSpellIndex = 0;

    // - Target
    private GameObject target = null;
    private Vector3 targetSkillshot = Vector3.zero;
    private bool isSkillshotting = false;
    private Vector3 targetLastKnownPosition = Vector3.zero;
    public int targetId = -1;
    public string targetTag = "";

    // - Movement
    NavMeshAgent agent = null;
    private NavMeshPath path;
    public Vector3[] waypoints;
    private GameObject autoAttackInstance;
    private bool isDead = false;

    public Vector3 lastSpawnPosition = Vector3.zero;

    // - Config
    private int updateInterval = 60;
    public enum PlayerState
    {
        idle,
        walk,
        chase,
        attack,
        cast
    }

    // TODO: multi dimensional array 0 - warr; 1 - mage; 2 - priest
    private string[,] gameClasses = { { "Fury", "Arms", "Prot" }, { "Fire", "Frost", "Arcane" }, { "Holy", "Discipline", "Shadow" } };
    private string[] classes = { "Warrior", "Mage", "Priest" };
    public List<GameObject> spells = new List<GameObject>();
    public List<int> inventory = new List<int>();
    public int gold = 10000;
    public int[] xpRequirementsPerLevel = { 150, 250, 350, 450, 550, 650, 750, 850, 950, 1050 };
    public int level = 1;
    public int currentXp = 0;
    public GameObject[] enemySectors;
    private int enemySectorIndex = 0;

    #endregion

    private void Start()
    {
        gravity *= Time.fixedDeltaTime * Time.fixedDeltaTime;
        moveSpeed *= Time.fixedDeltaTime;
        jumpSpeed *= Time.fixedDeltaTime;
        agent = this.GetComponent<NavMeshAgent>();
        path = new NavMeshPath();
        baseMovementSpeed = agent.speed;
        //transform.position.Set(transform.position.x, transform.position.y + 1f, transform.position.z);
        UpdateState(PlayerState.idle);
        //timeToGo = Time.fixedTime + 60.0f;
    }

    public void Initialize(int _id, string _username, int _selectedClass, int _selectedSpec, int _team)
    {
        id = _id;
        username = _username;
        selectedClass = _selectedClass;
        selectedSpec = _selectedSpec;
        team = _team;

        this.GetComponent<CombatStats>().Initialize();

        attackSpeed = this.GetComponent<CombatStats>().attackSpeed;
        AddClassSpells();
    }

    public void SetSpawnPosition(Vector3 _position)
    {
        lastSpawnPosition = _position;
    }

    public bool IsDead()
    {
        return this.isDead;
    }

    #region State Machine

    public void FixedUpdate()
    {
        if (IsDead()) return;

        switch (state)
        {
            case PlayerState.idle:
                RunIdle();
                break;
            case PlayerState.walk:
                RunWalk();
                break;
            case PlayerState.chase:
                RunChase();
                break;
            case PlayerState.attack:
                RunAttack();
                break;
            case PlayerState.cast:
                RunCast();
                break;
            default:
                break;
        }

        // TODO: Have a intervaled reconciliation call
        if (Time.frameCount % this.updateInterval != 0) return;

    }

    private void RunIdle()
    {
        // TODO: auto-aggro
    }

    private void RunWalk()
    {
        CombatStats playerStats = this.GetComponent<CombatStats>();
        if (playerStats.GetCC()[1] || playerStats.GetCC()[2]) return; // 0 - silence, 1 - stun, 2 - snare, 3 - slow
        Vector3 destination = this.waypoints[this.waypoints.Length - 1];
        Vector3 destinationToPlayer = destination - transform.position;

        // - Arrived -> Idle
        if (agent.isStopped == true || destinationToPlayer.magnitude <= 0.1f)
        {
            StopPath();
            UpdateState(PlayerState.idle);
            return;
        }
    }

    private void RunChase()
    {
        CombatStats playerStats = this.GetComponent<CombatStats>();
        if (playerStats.GetCC()[1] || playerStats.GetCC()[2]) return; // 0 - silence, 1 - stun, 2 - snare, 3 - slow
        if (this.isSkillshotting)
        {
            Vector3 _playerToTarget = this.targetSkillshot - transform.position;
            /*
            if (this.isCastChase && _playerToTarget.magnitude <= this.castChaseRange)
            {
                */
            RotatePlayer(_playerToTarget);
            StopPath();
            UpdateState(PlayerState.cast);
            this.isSkillshotting = false;
            return;
            /*}*/
        }
        else
        {
            Vector3 _playerToTarget = target.transform.position - transform.position;
            float targetRadius = target.GetComponent<CharacterController>().radius; //both enemy and players have character controllers

            // - In Range -> Attack
            if (!this.isCastChase && _playerToTarget.magnitude <= this.shootRange + targetRadius)
            {
                StopPath();
                UpdateState(PlayerState.attack);
                return;
            }

            // - In Range -> Cast
            if (this.isCastChase && _playerToTarget.magnitude <= this.castChaseRange + targetRadius)
            {
                RotatePlayer(_playerToTarget);
                StopPath();
                UpdateState(PlayerState.cast);
                return;
            }
        }

        // - Continue Chase
        MovePlayerAndSendPosition(this.target.transform.position);
    }

    private void RunAttack()
    {
        CombatStats playerStats = this.GetComponent<CombatStats>();
        if (playerStats.GetCC()[1]) return; // 0 - silence, 1 - stun, 2 - snare, 3 - slow
        if (target.gameObject == null)
        {
            UpdateState(PlayerState.idle);
            return;
        }
        Vector3 _playerToTarget = target.transform.position - transform.position;
        transform.forward = new Vector3(_playerToTarget.x, 0f, _playerToTarget.z);
        float targetRadius = target.GetComponent<CharacterController>().radius;

        // - Continue Attack
        if (_playerToTarget.magnitude <= shootRange + targetRadius)
        {
            RotatePlayer(_playerToTarget);
            if (!isAttackRoutineRunning && !isCasting) // ? Remove isCasting ?
            {

                StopPath();
                StartCoroutine(AutoAttack());
            }
            return;
        }

        // - Target out of range -> Chase
        UpdateState(PlayerState.chase);
    }

    private void RunCast()
    {
        CombatStats playerStats = this.GetComponent<CombatStats>();
        if (playerStats.GetCC()[0] || playerStats.GetCC()[1]) return; // 0 - silence, 1 - stun, 2 - snare, 3 - slow
        Spell castingSpell = this.spells[this.castSpellIndex].GetComponent<Spell>();
        float castTime = this.spells[this.castSpellIndex].GetComponent<Spell>().castTime; // Prune
        bool localIsSkillshot = false;
        // - Cast Once
        if (!this.isCasting && !castingSpell.isOnCooldown)
        {
            if (castingSpell is ProjectileSpell spell)
            {
                if (spell.isSkillshot)
                {
                    localIsSkillshot = true;
                    if (!spell.Cast(this.targetSkillshot, this.gameObject))// - Check if not castable
                    {
                        // - Finish Cast -> Idle
                        FinishCast(true);
                        return;
                    }
                }
            }

            if (castingSpell is AoeSpell aoeSpell)
            {
                if (aoeSpell.isSkillshot)
                {
                    localIsSkillshot = true;
                    if (!aoeSpell.Cast(this.targetSkillshot, this.gameObject))// - Check if not castable
                    {
                        // - Finish Cast -> Idle
                        FinishCast(true);
                        return;
                    }
                }
            }

            if (castingSpell is MovementSpell movementSpell)
            {
                if (movementSpell.isSkillshot)
                {
                    localIsSkillshot = true;
                    if (!movementSpell.Cast(this.targetSkillshot, this.gameObject))// - Check if not castable
                    {
                        // - Finish Cast -> Idle
                        FinishCast(true);
                        return;
                    }
                }
            }

            if (!localIsSkillshot && !castingSpell.Cast(this.target, this.gameObject))// - Check if not castable
            {
                // - Finish Cast -> Idle
                FinishCast(true);
                return;
            }


            if (castTime > 0)
            {
                this.isCasting = true;
                ServerSend.PlayerCastedStarted(id, castTime + 0.2f, castingSpell.castAnimation); //casttime + 200 ms compensation
                StartCoroutine(Casting(castTime + 0.2f)); //casttime + 200 ms compensation 
            }
            else
            {
                this.isCastChase = false;
                UpdateState(PlayerState.idle);
            }
            return;
        }

        // - Out of Range -> Chase (isCastChase)
        // TODO: Verify out of range, go to chase. In chase it should detect in range again and change back to cast and run "Cast Once"

        // - Finish Cast -> Idle
        FinishCast();
    }

    #endregion

    private void FinishCast(bool forceFinish = false)
    {
        if (isCasting || forceFinish)
        {
            this.isCastChase = false;
            this.castChaseRange = 0f;
            this.castSpellIndex = 0;
            UpdateState(PlayerState.idle);
            StopPath();
        }
    }

    public void SetHealth(int _maxHealth)
    {
        maxHealth = _maxHealth;
        health = _maxHealth;
    }

    private void AddClassSpells()
    {
        switch (selectedClass)
        {
            // Warrior
            case (int)Constants.Classes.warrior:
                this.SetAutoAttackResource("Spells/MeleeAttack", false);

                switch (selectedSpec)
                {
                    case (int)Constants.WarriorSpecs.fury:
                        break;
                    case (int)Constants.WarriorSpecs.arms:
                        this.AddSpellResource("Spells/Warrior/Arms/Overpower");
                        this.AddSpellResource("Spells/Warrior/Arms/MortalStrike");
                        this.AddSpellResource("Spells/Warrior/Arms/Execute");
                        break;
                    case (int)Constants.WarriorSpecs.protection:
                        this.AddSpellResource("Spells/Warrior/Prot/Devastate");
                        this.AddSpellResource("Spells/Warrior/Prot/ThunderClap");
                        this.AddSpellResource("Spells/Warrior/Prot/ShieldBlock");
                        break;
                }
                break;

            // Mage
            case (int)Constants.Classes.mage:
                this.SetAutoAttackResource("Spells/Mage/Fire/FireBasicAttack", true);

                switch (selectedSpec)
                {
                    case (int)Constants.MageSpecs.fire:
                        this.AddSpellResource("Spells/Mage/Fire/Fireball");
                        this.AddSpellResource("Spells/Mage/Fire/Fireblast");
                        this.AddSpellResource("Spells/Mage/Fire/Pyroblast");
                        break;
                    case (int)Constants.MageSpecs.frost:
                        this.AddSpellResource("Spells/Mage/Frost/Frostbolt");
                        this.AddSpellResource("Spells/Mage/Frost/IceLance");
                        this.AddSpellResource("Spells/Mage/Frost/GlacialSpike");
                        break;
                }
                break;

            // Priest
            case (int)Constants.Classes.priest:
                this.SetAutoAttackResource("Spells/Priest/Holy/PriestBasicAttack", true);

                switch (selectedSpec)
                {
                    case (int)Constants.PriestSpecs.holy:
                        this.AddSpellResource("Spells/Priest/Holy/Heal");
                        this.AddSpellResource("Spells/Priest/Holy/HolyFire");
                        this.AddSpellResource("Spells/Priest/Holy/Renew");
                        break;
                }
                break;

            // No class
            default:
                break;
        }

    }
    public void AddTalentSpell(int _spellId)
    {
        String playerClass = classes[selectedClass - 1];
        String playerSpec = gameClasses[selectedClass - 1, selectedSpec - 1];
        String spellName = SpellClientIdMap.ClientHotbarSpellsId.GetName(typeof(SpellClientIdMap.ClientHotbarSpellsId), _spellId);
        String resourcePath = $"Spells/{playerClass}/{playerSpec}/Talents/{spellName}";

        this.AddSpellResource(resourcePath);

        Debug.Log($"CAN ADD TALENT: {resourcePath}");
    }
    public void SetAutoAttackResource(string resourcePath, bool isRanged)
    {
        GameObject autoAttack = Resources.Load(resourcePath) as GameObject;
        autoAttackInstance = Instantiate(autoAttack, this.gameObject.transform);
        autoAttackInstance.SetActive(true);

        if (isRanged) this.shootRange = autoAttackInstance.GetComponent<RangedAttack>().range;
    }

    public void AddSpellResource(string resourcePath)
    {
        GameObject spellObject = Resources.Load(resourcePath) as GameObject;
        GameObject spellInstance = Instantiate(spellObject, this.gameObject.transform);
        spellInstance.SetActive(true);

        spells.Add(spellInstance);
    }

    public int GetSpellIndexById(int _spellId)
    {
        for (int i = 0; i < spells.Count; i++)
        {
            if ((int)spells[i].GetComponent<Spell>().id == _spellId)
            {
                return i;
            }
        }
        return -1;
    }

    // - Ability Commands
    public void TriggerAbility(int _clientHotbarSpellId, Vector3 _mouseTarget, string _mouseMode)
    {
        if (IsDead()) return;

        int castSpellIndex = GetSpellIndexById(_clientHotbarSpellId);
        Spell castSpell = spells[castSpellIndex].GetComponent<Spell>();

        this.target = GetTargetByPosition(_mouseTarget, _mouseMode);
        if (!this.target) return;

        if (castSpell.isSelfCast)
        {
            this.targetTag = "Ground";
        }
        else
        {
            this.targetTag = target.tag;
        }

        ServerLogging.instance.Debugger($"TARGET IS: {this.targetTag}, MOUSE MODE: {_mouseMode} , MOUSE TARGET: {_mouseTarget}");


        switch (target.tag)
        {
            case "Ground":
                // - Cast "support" spell on self
                if (castSpell.abilityType == Spell.AbilityType.heal || castSpell.isSelfCast)
                {
                    this.target = this.transform.gameObject;
                    targetId = this.id;
                    this.targetTag = target.tag;
                    // TODO: Server send target no target
                }
                else if (castSpell is ProjectileSpell projectileSpell)
                {
                    if (projectileSpell.isSkillshot)
                    {
                        this.targetId = -1; //ground
                        this.targetSkillshot = _mouseTarget;
                        this.isSkillshotting = true;
                    }
                }
                else if (castSpell is AoeSpell aoeSpell)
                {
                    if (aoeSpell.isSkillshot)
                    {
                        this.targetId = -1; //ground
                        this.targetSkillshot = _mouseTarget;
                        this.isSkillshotting = true;
                    }
                }
                else if (castSpell is MovementSpell moveSpell)
                {
                    if (moveSpell.isSkillshot)
                    {
                        this.targetId = -1; //ground
                        this.targetSkillshot = _mouseTarget;
                        this.isSkillshotting = true;
                    }
                }
                else return;

                break;
            case "Enemy":
                //if its a skillshot cast it on mouse target
                if (castSpell is ProjectileSpell projectileSpellOnEnemyTarget)
                {
                    if (projectileSpellOnEnemyTarget.isSkillshot)
                    {
                        this.targetId = -1; //ground
                        this.targetSkillshot = _mouseTarget;
                        this.isSkillshotting = true;
                    }
                }
                else if (castSpell is AoeSpell aoeSpell)
                {
                    if (aoeSpell.isSkillshot)
                    {
                        this.targetId = -1; //ground
                        this.targetSkillshot = _mouseTarget;
                        this.isSkillshotting = true;
                    }
                }
                else if (castSpell is MovementSpell moveSpell)
                {
                    if (moveSpell.isSkillshot)
                    {
                        this.targetId = -1; //ground
                        this.targetSkillshot = _mouseTarget;
                        this.isSkillshotting = true;
                    }
                }
                else
                {
                    this.targetId = target.GetComponent<Enemy>().id;
                }
                // TODO: Server send target selected
                break;
            case "Player":
                //if its a skillshot cast it on mouse target
                if (castSpell is ProjectileSpell projectileSpellOnPlayerTarget)
                {
                    if (projectileSpellOnPlayerTarget.isSkillshot)
                    {
                        this.targetId = -1; //ground
                        this.targetSkillshot = _mouseTarget;
                        this.isSkillshotting = true;
                    }
                }
                else
                {
                    targetId = target.GetComponent<Player>().id;
                }
                // TODO: Server send target selected
                break;
        }

        this.isCastChase = true;
        this.castChaseRange = castSpell.range;
        this.castSpellIndex = castSpellIndex;

        UpdateState(PlayerState.chase);
    }

    private IEnumerator Casting(float _castTime)
    {
        yield return new WaitForSeconds(_castTime);
        // TODO: PlayerCast
        isCasting = false;
        UpdateState(PlayerState.idle);
    }

    public void InterruptSpells()
    {
        StopCoroutine("Casting");
        isCasting = false;
        foreach (GameObject go in spells)
        {
            Spell s = go.GetComponent<Spell>();

            if (s.isCasting)
            {
                s.InterruptSpell();
            }
        }
    }

    private void UpdateState(PlayerState _state)
    {
        state = _state;
        ServerSend.PlayerState(this);
    }

    // - Mouse Commands
    public void MainAction(Vector3 _newPosition, string _mouseMode)
    {
        if (IsDead()) return;
        this.target = GetTargetByPosition(_newPosition, _mouseMode);
        if (!this.target) return;
        this.targetTag = target.tag;

        switch (target.tag)
        {
            case "Ground":
                UpdateState(PlayerState.walk);
                MovePlayerAndSendPosition(_newPosition);
                break;
            case "Enemy":
                this.targetId = target.GetComponent<Enemy>().id;
                UpdateState(PlayerState.chase);
                break;
            case "Player":
                targetId = target.GetComponent<Player>().id;
                if (targetId == this.id)
                {
                    UpdateState(PlayerState.idle);
                    return;
                }
                // Chase(true);
                UpdateState(PlayerState.chase);
                break;
        }

    }

    public void MovePlayerAndSendPosition(Vector3 _newPosition)
    {
        InterruptSpells();
        agent.isStopped = false;

        _newPosition.y = 1f;
        NavMesh.CalculatePath(transform.position, _newPosition, NavMesh.AllAreas, path);
        agent.SetPath(path);

        if (path.corners.Length > 1)
        {
            Quaternion rotationZeroX = Quaternion.LookRotation(path.corners[1] - this.transform.position);
            rotationZeroX.x = 0;
            this.transform.rotation = rotationZeroX;
        }

        waypoints = path.corners;

        ServerSend.PlayerPath(this);
    }

    public void StopPath()
    {
        agent.isStopped = true;
        waypoints = Array.Empty<Vector3>();
        ServerSend.PlayerStopped(this);
    }

    public NavMeshAgent GetAgent()
    {
        return agent;
    }

    public void BuyItem(int _itemId)
    {
        Debug.Log("ID " + UIItemDatabase.Instance.items[_itemId - 1].ID);
        Debug.Log("NAME " + UIItemDatabase.Instance.items[_itemId - 1].Name);

        int goldValue = UIItemDatabase.Instance.items[_itemId - 1].GoldValue;

        if (goldValue < gold)
        {
            gold -= goldValue;
            inventory.Add(_itemId);
            this.GetComponent<CombatStats>().AddItemStats(_itemId);
            ServerSend.PlayerBoughtItem(id, _itemId, gold);
        }
    }

    private GameObject GetTargetByPosition(Vector3 _newPosition, string _mouseMode)
    {
        _newPosition.y = 20f;
        switch (_mouseMode)
        {
            case "none":
                int groundMask = 1 << 9; // Ground Layer
                return OnSphereCast(_newPosition, groundMask);
            case "attack":
                // TODO: distinguish between enemy / player enemy AND ally  
                int enemyMask = 1 << 10; // Enemy Layer
                int attackPlayerMask = 1 << 11; // Player Layer
                int mask = enemyMask | attackPlayerMask;
                return OnSphereCast(_newPosition, mask);
            case "support":
                // TODO: distinguish between enemy / player enemy AND ally  
                int playerMask = 1 << 11; // Player Layer
                return OnSphereCast(_newPosition, playerMask);
            default:
                return null;
        }
    }

    private GameObject OnSphereCast(Vector3 _newPosition, int _mask)
    {
        if (Physics.SphereCast(_newPosition, 4f, new Vector3(0f, -1f, 0f), out RaycastHit _hitSphere, 100, _mask))
        {
            DrawPrimitives.instance.DrawBall(_newPosition);
            // ServerSend.DrawBall(_hitSphere.point, 4, "Clicked surface");

            return _hitSphere.transform.gameObject;
        }
        return null;
    }

    private IEnumerator AutoAttack()
    {
        isAttackRoutineRunning = true;
        autoAttackInstance.GetComponent<Spell>().Cast(target, this.gameObject);

        yield return new WaitForSeconds(this.GetComponent<CombatStats>().attackSpeed);
        isAttackRoutineRunning = false;
    }

    public void RotatePlayer(Vector3 _playerToTarget)
    {
        if (_playerToTarget == Vector3.zero) return;

        Quaternion rotation = Quaternion.LookRotation(_playerToTarget);
        rotation.x = 0;
        this.transform.rotation = rotation;
        ServerSend.PlayerRotation(this);
    }

    public Vector3 GetNextSector()
    {
        Vector3 nextEnemySector = enemySectors[enemySectorIndex].transform.position;
        
        if (enemySectorIndex > 7)
        {
            enemySectorIndex = 0;
        }
        else
        {
            enemySectorIndex++;
        }
        return nextEnemySector;
    }

    public void SetRewards(int _goldValue, int _xpValue)
    {
        AddGoldValue(_goldValue);
        AddXpValue(_xpValue);
    }
    public void AddGoldValue(int _goldValue)
    {
        gold += _goldValue;
        ServerSend.PlayerSetGold(id, gold);
    }
    public void AddXpValue(int _xpValue)
    {
        int xpRequiredToLevel = xpRequirementsPerLevel[level - 1];
        bool isLevelUp = false;
        /*
                for (int i = 0; i < level; i++)
                {
                    xpRequiredToLevel += xpRequirementsPerLevel[i];
                }
        */
        if (currentXp + _xpValue >= xpRequiredToLevel)
        {
            level++;
            currentXp = (currentXp + _xpValue) - xpRequiredToLevel;
            isLevelUp = true;
        }
        else
        {
            currentXp += _xpValue;
            isLevelUp = false;
        }
        ServerSend.PlayerSetXpLevel(id, level, currentXp, xpRequirementsPerLevel[level - 1], isLevelUp);
    }

    /*
    public void UpdateHealth(float _healthDif)
    {
        if (this.health <= 0f) return;

        float newHealth = this.health + _healthDif;
        if (newHealth <= 0f) Die();
        else if (newHealth >= this.maxHealth) this.health = maxHealth;
        else this.health = newHealth;

        ServerSend.PlayerHealth(this);
    }
    */

    public void TakeDamage(float _damage)
    {
        if (health <= 0f)
        {
            return;
        }

        float armor = this.GetComponent<CombatStats>().armor;
        //health -= _damage;
        health -= _damage * 100/(100 + armor);

        if (health <= 0f)
        {   
            bool hasDeathImmunity = this.GetComponent<CombatStats>().hasDeathImmunity;
            float healHealthPercentage = this.GetComponent<CombatStats>().healPercentage;
            if (!hasDeathImmunity)
            {
                Die();
            }
            else
            {
                HealDamage(this.maxHealth * healHealthPercentage); //try get spell heal value
            }
        }

        ServerSend.PlayerHealth(this);
    }

    private void Die()
    {
        this.health = 0f;
        this.isDead = true;

        UpdateState(PlayerState.idle);
        agent.Warp(lastSpawnPosition);
        StopPath();
        StartCoroutine(Respawn());
    }

    // ! DELETE
    public void HealDamage(float _healAmount)
    {
        float healingTaken = this.GetComponent<CombatStats>().healingTaken;

        if (health <= 0f)
        {
            return;
        }

        health += _healAmount * (1 + healingTaken);
        if (health >= maxHealth)
        {
            health = maxHealth;
        }

        ServerSend.PlayerHealth(this);
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(5f);

        health = maxHealth;
        //controller.enabled = true;
        isDead = false;
        ServerSend.PlayerRespawned(this);
    }
}