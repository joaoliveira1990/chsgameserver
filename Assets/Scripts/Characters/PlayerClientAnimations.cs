﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerClientAnimations
{
    public static string noAttackAnimation = ""; //if it's a cast
    public static string basicAttack = "attack_01";
    public static string attack2 = "attack_02";
    public static string cast = "cast_01";
}
