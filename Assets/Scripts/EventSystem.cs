﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public class EventSystem : MonoBehaviour
{
    private int timer = 0; // In seconds
    public EnemySpawner[] dungeon1EnemySpawns;
    public EnemySpawner[] dungeon2EnemySpawns;
    public EnemySpawner enemyEliteSpawnerDungeon1;
    public EnemySpawner enemyBossSpawnerDungeon1;
    public EnemySpawner enemyDragonBossSpawnerDungeon1;
    public EnemySpawner enemyEliteSpawnerDungeon2;
    public EnemySpawner enemyBossSpawnerDungeon2;
    public EnemySpawner enemyDragonBossSpawnerDungeon2;
    private Enemy bossDungeon1;
    private Enemy bossDungeon2;
    public float clockTimeLapse = 0.0f;
    public float clockPeriodUpdate = 1.0f;
    public System.DateTime startTime = System.DateTime.UtcNow;
    private bool wasReset = false;
    private float intermissionZeroTime = 0f;
    public float timeToBeginRound = 0f;
    public float timeToEndRound = 0f;
    private float pveRoundTime = 0f;
    public float intermissionTime = 0f;
    private int team1MemberCount = 0;
    private int team2MemberCount = 0;
    private bool canCheckhealth = true;

    private Transform[] playerSpawnersHubTeam1;
    private Transform[] playerSpawnersHubTeam2;
    private Transform[] playerSpawnersArenaTeam1;
    private Transform[] playerSpawnersArenaTeam2;

    public enum EventState
    {
        intermissionZero,
        pveRound,
        intermission,
        pvpRound
    }
    private EventState state;

    private void UpdateState(EventState _state)
    {
        state = _state;
        //ServerSend.EventState(this); ??? why not
    }

    private void Start()
    {
        playerSpawnersHubTeam1 = NetworkManager.instance.playerSpawnersAHub;
        playerSpawnersHubTeam2 = NetworkManager.instance.playerSpawnersBHub;

        playerSpawnersArenaTeam1 = NetworkManager.instance.playerSpawnersAArena1;
        playerSpawnersArenaTeam2 = NetworkManager.instance.playerSpawnersBArena1;
    }
    private void Update()
    {
        // - Update clock
        if (this.clockTimeLapse > this.clockPeriodUpdate)
        {
            System.TimeSpan ts = System.DateTime.UtcNow - startTime;
            this.timer = (int)ts.TotalSeconds;
            this.clockTimeLapse = 0;
        }
        this.clockTimeLapse += UnityEngine.Time.deltaTime;

        // - Event Triggers
        if (wasReset) EventTriggers();
    }

    private void EventTriggers()
    {
        switch (state)
        {
            case EventState.intermissionZero:
                RunIntermissionZero();
                break;
            case EventState.pveRound:
                RunPveRound();
                break;
            case EventState.intermission:
                RunIntermission();
                break;
            case EventState.pvpRound:
                RunPvpRound();
                break;
        }
    }

    private void RunIntermissionZero()
    {
        if (timer >= intermissionZeroTime + timeToBeginRound)
        {
            TransitionToPveRound();
        }
    }
    private void TransitionToPveRound()
    {
        Debug.Log("Teleport players to Dungeon and spawn enemies");
        SetTeamMembersCount();//this should be at the start of the game 
        TeleportAllPlayersToDungeon();

        SpawnDungeonEnemies(dungeon1EnemySpawns);
        SpawnDungeonEnemies(dungeon2EnemySpawns);

        this.bossDungeon1 = enemyDragonBossSpawnerDungeon1.SpawnEnemy(EnemyTypes.dragonBoss).GetComponent<Enemy>();
        this.bossDungeon2 = enemyDragonBossSpawnerDungeon2.SpawnEnemy(EnemyTypes.dragonBoss).GetComponent<Enemy>();

        UpdateState(EventState.pveRound);
        //SetEventTimer(timeToEndRound) serversend to all clients like ResetTimer
    }

    private void RunPveRound()
    {
        CheckBossesHealth();
        if (timer >= intermissionZeroTime + timeToEndRound)
        {
            TransitionToIntermission();
        }
    }
    private void TransitionToIntermission()
    {
        Debug.Log("Start Intermission before pvp");
        TeleportAllPlayersToArea(playerSpawnersHubTeam1, playerSpawnersHubTeam2);
        UpdateState(EventState.intermission);
        //SetEventTimer(intermissionTime) serversend to all clients like ResetTimer
    }
    private void RunIntermission()
    {
        if (timer >= intermissionZeroTime + timeToEndRound + intermissionTime)
        {
            TransitionToPvpRound();
        }
    }
    private void TransitionToPvpRound()
    {
        Debug.Log("Start pvp");
        this.timeToEndRound = this.pveRoundTime;
        TeleportAllPlayersToArea(playerSpawnersArenaTeam1, playerSpawnersArenaTeam2);
        UpdateState(EventState.pvpRound);
        //SetEventTimer(0f) serversend to all clients like ResetTimer
    }

    private void RunPvpRound()
    {
        if (PlayersDead()) TransitionToIntermissionZero();
    }
    private void TransitionToIntermissionZero()
    {
        Debug.Log("Intermission 0");
        EndPvpRound();
        TeleportAllPlayersToArea(playerSpawnersHubTeam1, playerSpawnersHubTeam2);
        UpdateState(EventState.intermissionZero);
        //SetEventTimer(0f) serversend to all clients like ResetTimer
    }

    private void SpawnDungeonEnemies(EnemySpawner[] spawners)
    {
        spawners[0].SpawnEnemy(EnemyTypes.meleeBoss);

        for (int i = 1; i < spawners.Length; i++)
        {
            if (i <= spawners.Length - 2)
            {
                if (i % 2 == 0) spawners[i].SpawnEnemy(EnemyTypes.rangedNormal);
                else spawners[i].SpawnEnemy(EnemyTypes.meleeNormal);//change name to melee
            }
            else
            {
                spawners[i].SpawnEnemy(EnemyTypes.rangedBoss);
            }
        }
    }

    public void EndPvpRound()
    {
        //reset
        this.intermissionZeroTime = this.timer;
        this.canCheckhealth = true;
        RemoveAllEnemies();
    }

    private void RemoveAllEnemies()
    {
        foreach (Enemy enemy in Enemy.enemies.Values)
        {
            enemy.Suicide(); //kill them first 
        }
        Enemy.enemies.Clear(); //then remove them from DIC
    }

    private void TeleportAllPlayersToDungeon()
    {
        int spawnPointArena1Index = 0;
        int spawnPointArena2Index = 0;

        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                var agent = _client.player.GetAgent();
                agent.isStopped = true;
                Vector3 spawnPosition = Vector3.zero;

                if (_client.player.team == 1)
                {
                    spawnPosition = NetworkManager.instance.playerSpawnersDungeon1[spawnPointArena1Index].position;
                    agent.Warp(spawnPosition);
                    _client.player.SetSpawnPosition(spawnPosition);
                    spawnPointArena1Index++;
                }
                else
                {
                    spawnPosition = NetworkManager.instance.playerSpawnersDungeon2[spawnPointArena2Index].position;
                    agent.Warp(spawnPosition);
                    _client.player.SetSpawnPosition(spawnPosition);
                    spawnPointArena2Index++;
                }

                ServerSend.PlayerWarp(_client.player, _client.player.transform.position);
            }
        }
    }

    private void TeleportAllPlayersToArea(Transform[] spawnerTeam1, Transform[] spawnerTeam2)
    {
        int spawnPointArena1Index = 0;
        int spawnPointArena2Index = 0;

        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                var agent = _client.player.GetAgent();
                agent.isStopped = true;
                Vector3 spawnPosition = Vector3.zero;

                if (_client.player.team == 1)
                {
                    spawnPosition = spawnerTeam1[spawnPointArena1Index].position;
                    agent.Warp(spawnPosition);
                    _client.player.SetSpawnPosition(spawnPosition);
                    spawnPointArena1Index++;
                }
                else
                {
                    spawnPosition = spawnerTeam2[spawnPointArena2Index].position;
                    agent.Warp(spawnPosition);
                    _client.player.SetSpawnPosition(spawnPosition);
                    spawnPointArena2Index++;
                }

                ServerSend.PlayerWarp(_client.player, _client.player.transform.position);
            }
        }
    }

    private void CheckBossesHealth()
    {
        if (this.bossDungeon1 == null && this.bossDungeon2 == null && this.canCheckhealth) //if bosses are spawned
        {
            this.pveRoundTime = this.timeToEndRound;
            this.timeToEndRound = this.timer + 3f - this.intermissionZeroTime;
            this.canCheckhealth = false;
        }
    }

    private bool PlayersDead()
    {
        int team1DeadCount = 0;
        int team2DeadCount = 0;

        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                if (_client.player.team == 1)
                {
                    if (_client.player.IsDead())
                    {
                        team1DeadCount++;
                    }
                }
                else
                {
                    if (_client.player.IsDead())
                    {
                        team2DeadCount++;
                    }
                }
            }
        }

        if (team1DeadCount == this.team1MemberCount)
        {
            //team 1 dead
            return true;
        }

        if (team2DeadCount == this.team2MemberCount)
        {
            //team 2 dead
            return true;
        }
        return false;
    }

    private void SetTeamMembersCount()
    {
        this.team1MemberCount = 0;
        this.team2MemberCount = 0;

        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                if (_client.player.team == 1)
                {
                    this.team1MemberCount++;
                }
                else
                {
                    this.team2MemberCount++;
                }
            }
        }
    }

    public void StopEvents()
    {
        this.wasReset = false;
    }

    public void OnClickResetTimer()
    {
        ResetTimer(0, true);
    }
    public void ResetTimer(int offSet, bool update = true)
    {
        this.startTime = System.DateTime.UtcNow;
        this.timer = 0;
        if (update) ServerSend.ResetTimer(true);
        // TODO: Reset everything (enemies, player position, event flags, etc)

        this.wasReset = true;
        UpdateState(EventState.intermissionZero);
    }
}
