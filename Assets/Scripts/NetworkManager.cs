﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;

    public GameObject playerPrefab;
    //public GameObject enemyPrefab;
    //public GameObject enemyRangedPrefab;
    public GameObject[] enemyPrefabs;
    public GameObject dummyPrefab;
    public GameObject projectilePrefab;

    public Transform[] playerSpawnersAHub = new Transform[5];
    public Transform[] playerSpawnersBHub = new Transform[5];

    public Transform[] playerSpawnersDungeon1 = new Transform[5];
    public Transform[] playerSpawnersDungeon2 = new Transform[5];

    public Transform[] playerSpawnersAArena1 = new Transform[5];
    public Transform[] playerSpawnersBArena1 = new Transform[5];

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        Server.Start(50, 26950);
    }

    private void OnApplicationQuit()
    {
        Server.Stop();
    }

    public Player InstantiatePlayer(int playerId, int _team)
    {
        // esta parte devia estar no event system? 
        // TODO: Protect "id" using to access array
        Vector3 playerSpawnPosition = Vector3.zero;

        if (_team == 1)
        {
            playerSpawnPosition = playerSpawnersAHub[playerId - 1].transform.position;
        }
        else
        {
            playerSpawnPosition = playerSpawnersBHub[playerId - 1].transform.position;
        }

        return Instantiate(playerPrefab, playerSpawnPosition, Quaternion.identity).GetComponent<Player>();
    }

    public GameObject InstantiateEnemy(Vector3 _position, int _enemyType)
    {
        GameObject enemyObj = Instantiate(enemyPrefabs[_enemyType], _position, Quaternion.identity);
        enemyObj.GetComponent<NavMeshAgent>().radius = 0.0001f;
        return enemyObj;
    }

    public Projectile InstantiateProjectile(Transform _shootOrigin)
    {
        return Instantiate(projectilePrefab, _shootOrigin.position + _shootOrigin.forward * 0.7f, Quaternion.identity).GetComponent<Projectile>();
    }
}
