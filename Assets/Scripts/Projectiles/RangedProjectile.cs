﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Attach to projectile to make guided missile that self destroys on collision with target
 * Analogous script in client. Projectiles run in parallel in client - server
 */
public class RangedProjectile : MonoBehaviour
{
    /* Target to be followed until collision w/ projectile */
    private GameObject target = null;
    private Vector3 targetSkillshot = Vector3.zero;
    private Vector3 casterPosition = Vector3.zero;
    private bool isSkillshot = false;
    private Vector3 initialPosition = Vector3.zero;
    private Effect effect = null;
    private float skillshotRange = 0f;
    /* Update projectile once target is set */
    private bool isTargetSet = false;
    private int playerId = 0;
    private int casterTeam = 0;
    private float threatModifier = 0f;

    public float chaseSpeed = 6.0f;
    public float hitRange = 2.0f;
    private float damage = 0f;
    private ProjectileSpell parentSpell;

    private float targetRadius = 0f;

    public void Initialize(GameObject _target, float _damage)
    {
        this.target = _target;
        this.isTargetSet = true;
        this.damage = _damage;
    }

    //skillshot
    public Vector3 Initialize(ProjectileSpell _spell, Vector3 _casterPosition, Vector3 _target, float _damage, int _playerId, float _threatModifier, float _range, int _team, Effect _effect)
    {
        this.targetSkillshot = _target;
        this.casterPosition = _casterPosition;
        this.targetSkillshot.y = transform.position.y;
        this.isTargetSet = false;
        this.playerId = _playerId;
        this.casterTeam = _team;
        this.threatModifier = _threatModifier;
        this.damage = _damage;
        this.isSkillshot = true;
        this.skillshotRange = _range;
        this.initialPosition = transform.position;
        this.effect = _effect;
        this.parentSpell = _spell;

        //if skillshot then turn collider on
        //this.GetComponent<CapsuleCollider>().enabled = true;

        //direction vector must be calculated from the caster position 
        //and not from shootOrigin or the projectile will be shot backwards
        Vector3 distance = this.targetSkillshot - this.casterPosition;

        Debug.DrawRay(this.casterPosition, distance, Color.green, 10f);

        //if mouse position is less than the range, increase 
        //mouse position in the direction  between target and caster
        //so the projectile will go to mouseposition + X where x > remaningRange
        Debug.Log($"INITIAL DISTANCE {distance.magnitude}");
        if (distance.magnitude < _range)
        {
            distance.Normalize();
            this.targetSkillshot += distance * (_range * 2);//offset so we can make sure its destroyed
        }

        Debug.Log($"NEW DISTANCE {(this.targetSkillshot - this.casterPosition).magnitude}");
        return this.targetSkillshot;
    }

    //follow target
    public void Initialize(GameObject _target, float _damage, int _playerId, float _threatModifier)
    {
        this.target = _target;
        this.isTargetSet = true;
        this.damage = _damage;
        this.playerId = _playerId;
        this.threatModifier = _threatModifier;

        if (_target.TryGetComponent<CombatStatsEnemy>(out CombatStatsEnemy cse))
        {
            this.targetRadius = cse.GetHitboxRadius();
        }
    }

    private void FixedUpdate()
    {
        if (this.isSkillshot)
        {
            Vector3 targetToPos = this.targetSkillshot - this.casterPosition;
            Vector3 targetToPosHoriz = new Vector3(targetToPos.x, 0f, targetToPos.z);

            GuidedMovementUpdate(targetToPosHoriz);
            CheckSkillshotCollision();
        }
        else
        {
            // Destroy if target dies
            if (this.target == null && this.isTargetSet) Destroy(this.gameObject);
            if (this.target == null) return;

            Vector3 targetToPos = target.transform.position - transform.position;
            Vector3 targetToPosHoriz = new Vector3(targetToPos.x, 0f, targetToPos.z);

            GuidedMovementUpdate(targetToPosHoriz);
            CheckColision(targetToPosHoriz);
        }
    }

    /* Movement update based on horizontal projection of vector to target */
    private void GuidedMovementUpdate(Vector3 targetToPosHoriz)
    {
        transform.forward = targetToPosHoriz;
        Vector3 _movement = transform.forward * chaseSpeed;
        targetToPosHoriz = new Vector3(targetToPosHoriz.x, 0f, targetToPosHoriz.z);

        this.GetComponent<CharacterController>().Move(_movement);

        Vector3 lookMovement = _movement;
        transform.rotation = Quaternion.LookRotation(lookMovement);
    }

    /* Check collision using horizontal projection of vector to target */
    private void CheckColision(Vector3 targetToPosHoriz)
    {
        if (targetToPosHoriz.magnitude - this.targetRadius <= hitRange)
        {
            if (target.TryGetComponent(out Enemy e)) e.TakeDamage(this.damage, playerId, threatModifier);
            else if (target.TryGetComponent(out Player p)) p.TakeDamage(this.damage);

            Destroy(this.gameObject);
        }
    }

    private void CheckSkillshotCollision()
    {
        float currentMagnitude = (this.initialPosition - transform.position).magnitude;
        //Debug.Log("MAG " + currentMagnitude);
        if (currentMagnitude > this.skillshotRange)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("HIT - t");
        Debug.Log(collision.gameObject.tag);
        GameObject target = collision.gameObject;

        if (target.transform.parent.TryGetComponent(out Enemy e))
        {
            this.parentSpell.SpellHit(e, playerId, threatModifier);
            /*
            e.TakeDamage(this.damage, playerId, threatModifier); //playerid and threat for aggro calculations
            if (effect != null)
            {
                e.GetComponent<CombatStatsEnemy>().AddEffect(effect);
            }
            this.parentSpell.timesItHit++;
            */
            Destroy(this.gameObject);
        }
        else if (target.transform.parent.TryGetComponent(out Player p))
        {
            if (p.team != this.casterTeam)
            {
                this.parentSpell.SpellHit(p);
                /*
                p.TakeDamage(this.damage);
                if (effect != null)
                {
                    p.GetComponent<CombatStats>().AddEffect(effect);
                }
                this.parentSpell.timesItHit++;
                */
                Destroy(this.gameObject);
            }
            else
            {
                return;
            }
        }
        Debug.Log(this.parentSpell.timesItHit);
    }
}
