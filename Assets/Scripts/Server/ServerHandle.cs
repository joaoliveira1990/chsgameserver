﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle
{
    public static void WelcomeReceived(int _fromClient, Packet _packet)
    {
        int _clientIdCheck = _packet.ReadInt();
        string _username = _packet.ReadString();
        int _selectedClass = _packet.ReadInt();
        int _selectedSpec = _packet.ReadInt();
        int _team = _packet.ReadInt();

        ServerLogging.instance.Debugger($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint}");
        ServerLogging.instance.Debugger($"connected successfully and is now player {_fromClient}.");

        if (_fromClient != _clientIdCheck)
        {
            ServerLogging.instance.Debugger($"Player \"{_username}\" (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})! OOPS");
        }

        //TODO "middleware" para escolher uma personagem
        ServerLogging.instance.Debugger($"Sending player configuration to {_clientIdCheck}");
        ServerLogging.instance.Debugger($"selected class : {_selectedClass} selected spec : {_selectedSpec}");

        //spawn the player
        Server.clients[_fromClient].SendIntoGame(_username, _selectedClass, _selectedSpec, _team);

        //ServerSend.PlayerConfiguration(_clientIdCheck, _selectedChampion);
    }
    /*
    public static void PlayerMovement(int _fromClient, Packet _packet)
    {
        bool[] _inputs = new bool[_packet.ReadInt()];
        for (int i = 0; i < _inputs.Length; i++)
        {
            _inputs[i] = _packet.ReadBool();
        }
        Quaternion _rotation = _packet.ReadQuaternion();

        Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
    }
    */

    public static void PlayerAction(int _fromClient, Packet _packet)
    {
        Vector3 newPosition = _packet.ReadVector3();
        string mouseMode = _packet.ReadString();
        //Quaternion _rotation = _packet.ReadQuaternion();

        //Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
        DrawPrimitives.instance.DrawVerticalRay(newPosition, 20.0f);
        DrawPrimitives.instance.DrawBall(newPosition);
        Server.clients[_fromClient].player.MainAction(newPosition, mouseMode);
    }

    public static void PlayerShoot(int _fromClient, Packet _packet)
    {
        Vector3 _shootDirection = _packet.ReadVector3();

        //Server.clients[_fromClient].player.Shoot(_shootDirection);
    }

    public static void PlayerThrow(int _fromClient, Packet _packet)
    {
        Vector3 _throwDirection = _packet.ReadVector3();

        // Server.clients[_fromClient].player.ThrowItem(_throwDirection);
    }

    public static void TriggerAbility(int _fromClient, Packet _packet)
    {
        int _id = _packet.ReadInt(); // ? What to do with this ?
        int _clientHotbarSpellId = _packet.ReadInt();
        Vector3 _mouseTarget = _packet.ReadVector3();
        string _mouseMode = _packet.ReadString();

        Server.clients[_fromClient].player.TriggerAbility(_clientHotbarSpellId, _mouseTarget, _mouseMode);
    }

    public static void BuyItem(int _fromClient, Packet _packet)
    {
        int _id = _packet.ReadInt();
        int _itemId = _packet.ReadInt();

        Server.clients[_id].player.BuyItem(_itemId);
    }
    public static void AddTalent(int _fromClient, Packet _packet)
    {
        int _id = _packet.ReadInt();
        int _spellId = _packet.ReadInt();

        Server.clients[_id].player.AddTalentSpell(_spellId);
    }
}
