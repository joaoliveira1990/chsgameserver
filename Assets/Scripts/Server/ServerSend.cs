﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerSend
{

    public static void Welcome(int _toClient, string _msg)
    {
        using (Packet _packet = new Packet((int)ServerPackets.welcome))
        {
            _packet.Write(_msg);
            _packet.Write(_toClient);

            SendTCPData(_toClient, _packet);
        }
    }

    #region Send Functions

    private static void SendTCPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].tcp.SendData(_packet);
    }

    private static void SendUDPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].udp.SendData(_packet);
    }

    private static void SendTCPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].tcp.SendData(_packet);
        }
    }

    private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].tcp.SendData(_packet);

            }
        }
    }

    private static void SendUDPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].udp.SendData(_packet);
        }
    }

    private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].udp.SendData(_packet);

            }
        }
    }

    #endregion

    #region Player

    public static void PlayerConfiguration(int _clientId, int _selectedChampion)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerConfiguration))
        {
            _packet.Write(_clientId);
            _packet.Write(_selectedChampion);

            SendTCPDataToAll(_clientId, _packet);
        }
    }

    public static void SpawnPlayer(int _toClient, Player _player)
    {
        ServerLogging.instance.Debugger($"SENDING ... {_player.transform.position}");

        using (Packet _packet = new Packet((int)ServerPackets.spawnPlayer))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.username);
            _packet.Write(_player.selectedClass);
            _packet.Write(_player.selectedSpec);
            _packet.Write(_player.team);
            _packet.Write(_player.transform.position);
            _packet.Write(_player.transform.rotation);
            _packet.Write(_player.health);
            _packet.Write(_player.GetComponent<CombatStats>().mana);
            _packet.Write(_player.gold);

            SendTCPData(_toClient, _packet);
        }
    }

    public static void PlayerPath(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerPath))
        {
            _packet.Write(_player.waypoints.Length);
            _packet.Write(_player.id);

            foreach (Vector3 _waypoint in _player.waypoints)
            {
                _packet.Write(_waypoint);
            }

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerPosition(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerPosition))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.position);

            SendUDPDataToAll(_packet);
        }
    }

    public static void PlayerWarp(Player _player, Vector3 _position)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerWarp))
        {
            _packet.Write(_player.id);
            _packet.Write(_position);

            SendUDPDataToAll(_packet);
        }
    }

    public static void PlayerRotation(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerRotation))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.rotation);

            SendUDPDataToAll(_packet);
        }
    }

    public static void PlayerDisconnected(int _playerId)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerDisconnected))
        {
            _packet.Write(_playerId);
            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerHealth(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerHealth))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.health);
            _packet.Write(_player.maxHealth);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerCasted(int _casterId, float _castTime)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerCasted))
        {
            _packet.Write(_casterId);
            _packet.Write(_castTime);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerCastedStarted(int _casterId, float _castTime, string _animString)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerCastedStart))
        {
            _packet.Write(_casterId);
            _packet.Write(_castTime);
            _packet.Write(_animString);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerMana(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerMana))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.GetComponent<CombatStats>().mana);
            _packet.Write(_player.GetComponent<CombatStats>().maxMana);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerRage(Player _player)
    {
        Debug.Log($"PlayerRage: {_player.GetComponent<CombatStats>().rage}");
        using (Packet _packet = new Packet((int)ServerPackets.playerRage))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.GetComponent<CombatStats>().rage);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerRespawned(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerRespawned))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.lastSpawnPosition);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerState(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerState))
        {
            _packet.Write(_player.id);
            _packet.Write((int)_player.state);
            _packet.Write(_player.targetId);
            _packet.Write(_player.targetTag);

            SendUDPDataToAll(_packet);
        }
    }

    public static void PlayerAttacked(Player _player, string _targetType, int _targetid, string _animString, Constants.VfxOnHit _vfxHitId, Constants.VfxOnHit _vfxCasterId)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerAttacked))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.GetComponent<CombatStats>().attackSpeed);
            _packet.Write(_targetType);
            _packet.Write(_targetid);
            _packet.Write(_animString);
            _packet.Write((int)_vfxHitId);
            _packet.Write((int)_vfxCasterId);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerStopped(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerStopped))
        {
            _packet.Write(_player.id);

            SendUDPDataToAll(_packet);
        }
    }

    // TODO: Do the same for enemy
    public static void PlayerAddEffect(int _id, Constants.AllEffects _effectId, float _lifeTime, int _stacks, string _type)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerAddEffect))
        {
            _packet.Write(_id);
            _packet.Write(_lifeTime);
            _packet.Write((int)_effectId);
            _packet.Write(_stacks);
            _packet.Write(_type);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerRemoveEffect(int _playerId, Constants.AllEffects _effectId)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerRemoveEffect))
        {
            _packet.Write(_playerId);
            _packet.Write((int)_effectId);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerBoughtItem(int _playerId, int _itemId, int _gold)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerBoughtItem))
        {
            _packet.Write(_playerId);
            _packet.Write(_itemId);
            _packet.Write(_gold);

            SendTCPData(_playerId, _packet);
        }
    }
    public static void PlayerSetGold(int _playerId, int _gold)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerSetGold))
        {
            _packet.Write(_playerId);
            _packet.Write(_gold);

            SendTCPData(_playerId, _packet);
        }
    }
    public static void PlayerSetXpLevel(int _playerId, int _level, int _currentXp, int _xpRequiredToLevel, bool _isLevelUp)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerSetXpLevel))
        {
            _packet.Write(_playerId);
            _packet.Write(_level);
            _packet.Write(_currentXp);
            _packet.Write(_xpRequiredToLevel);
            _packet.Write(_isLevelUp);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerSetSpeed(int _playerId, float _speed)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerSetSpeed))
        {
            _packet.Write(_playerId);
            _packet.Write(_speed);

            SendTCPDataToAll(_packet);
        }
    }

    public static void PlayerIncreaseSizeForDuration(int _playerId, float _sizeMultiplier, float _duration)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerIncreaseSizeForDuration))
        {
            _packet.Write(_playerId);
            _packet.Write(_sizeMultiplier);
            _packet.Write(_duration);

            SendTCPDataToAll(_packet);
        }
    }
    

    public static void ShowAbilityCooldown(int _playerId, float _cooldown, int _hotbarIndex)
    {
        /*
        using (Packet _packet = new Packet((int)ServerPackets.showAbilityCooldown))
        {
            _packet.Write(_playerId);
            _packet.Write(_cooldown);
            _packet.Write(_hotbarIndex);

            SendTCPData(_playerId, _packet);
        }
        */
    }

    public static void UpdateStatWindow(int _playerId, int _strength, int _intellect, int _agility, int _stamina, int _armor, int _magicResistance, int _healthRegenValue, int _manaRegenValue, float _critChance, float _attackSpeed, float _damage, float _spellDamage)
    {
        using (Packet _packet = new Packet((int)ServerPackets.updateStatWindow))
        {
            _packet.Write(_strength);
            _packet.Write(_intellect);
            _packet.Write(_agility);
            _packet.Write(_stamina);
            _packet.Write(_armor);
            _packet.Write(_magicResistance);
            _packet.Write(_healthRegenValue);
            _packet.Write(_manaRegenValue);
            _packet.Write(_critChance);
            _packet.Write(_attackSpeed);
            _packet.Write(_damage);
            _packet.Write(_spellDamage);

            SendTCPData(_playerId, _packet);
        }
    }

    #endregion

    #region Enemy

    public static void EnemyPath(Enemy _enemy, bool _isStopped)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enemyPath))
        {
            _packet.Write(_isStopped);
            _packet.Write(_enemy.waypoints.Length);
            _packet.Write(_enemy.id);

            foreach (Vector3 _waypoint in _enemy.waypoints)
            {
                _packet.Write(_waypoint);
            }

            SendTCPDataToAll(_packet);
        }
    }

    public static void SpawnEnemy(Enemy _enemy)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnEnemy))
        {
            SendTCPDataToAll(SpawnEnemy_Data(_enemy, _packet));
        }
    }

    public static void SpawnEnemy(int _toClient, Enemy _enemy)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnEnemy))
        {
            SendTCPData(_toClient, SpawnEnemy_Data(_enemy, _packet));
        }
    }

    private static Packet SpawnEnemy_Data(Enemy _enemy, Packet _packet)
    {
        _packet.Write(_enemy.id);
        _packet.Write((int)_enemy.enemyType);
        _packet.Write(_enemy.transform.position);
        _packet.Write(_enemy.maxHealth);
        return _packet;
    }

    public static void EnemyPosition(Enemy _enemy)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enemyPosition))
        {
            _packet.Write(_enemy.id);
            _packet.Write(_enemy.transform.position);

            SendUDPDataToAll(_packet);
        }
    }
    public static void EnemyRotation(Enemy _enemy)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enemyRotation))
        {
            _packet.Write(_enemy.id);
            _packet.Write(_enemy.transform.rotation);

            SendUDPDataToAll(_packet);
        }
    }

    public static void EnemyState(Enemy _enemy)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enemyState))
        {
            _packet.Write(_enemy.id);
            _packet.Write((int)_enemy.state);

            SendUDPDataToAll(_packet);
        }
    }

    public static void EnemyAttacked(Enemy _enemy)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enemyAttacked))
        {
            _packet.Write(_enemy.id);
            _packet.Write(_enemy.attackSpeed);

            SendUDPDataToAll(_packet);
        }
    }

    public static void EnemyCastStarted(int _casterId, float _castTime, string _castAnimation)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enemyCastStart))
        {
            _packet.Write(_casterId);
            _packet.Write(_castTime);
            _packet.Write(_castAnimation);

            SendTCPDataToAll(_packet);
        }
    }

    public static void EnemyHealth(Enemy _enemy)
    {
        using (Packet _packet = new Packet((int)ServerPackets.enemyHealth))
        {
            _packet.Write(_enemy.id);
            _packet.Write(_enemy.health);

            SendTCPDataToAll(_packet);
        }
    }
    public static void EnemySetSpeed(int _enemyId, float _speed)
    {
        using (Packet _packet = new Packet((int) ServerPackets.enemySetSpeed))
        {
            _packet.Write(_enemyId);
            _packet.Write(_speed);

            SendTCPDataToAll(_packet);
        }
    }
    public static void SpawnProjectile(Vector3 _position, GameObject _target, Constants.ProjectileModel _projectileId)
    {
        int targetId = -1;
        string type = null;

        if (_target.TryGetComponent(out Player p))
        {
            targetId = p.id;
            type = "player";
        }
        else if (_target.TryGetComponent(out Enemy e))
        {
            targetId = e.id;
            type = "enemy";
        }
        else return;

        using (Packet _packet = new Packet((int)ServerPackets.spawnProjectile))
        {
            _packet.Write(targetId);
            _packet.Write(type);
            _packet.Write(_position);
            _packet.Write((int)_projectileId);
            _packet.Write(Vector3.zero);//not a skillshot
            _packet.Write(0f);//not a skillshot
            _packet.Write(0);//team

            SendUDPDataToAll(_packet);
        }
    }

    public static void SpawnProjectile(Vector3 _position, GameObject _target, Constants.ProjectileModel _projectileId, Vector3 _targetPosition, float _range, int _team)
    {
        //skillshot
        using (Packet _packet = new Packet((int)ServerPackets.spawnProjectile))
        {
            _packet.Write(-1);
            _packet.Write("ground");
            _packet.Write(_position);
            _packet.Write((int)_projectileId);
            _packet.Write(_targetPosition);
            _packet.Write(_range);
            _packet.Write(_team);

            SendUDPDataToAll(_packet);
        }
    }

    #endregion


    //added parentId parameter so we can have cone Aoes follow target
    public static void SpawnAoe(Vector3 _spawnPosition, float _radius, float _destroyAfter, int _aoeModel, int _parentId, string _parentType)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnAoe))
        {
            _packet.Write(_spawnPosition);
            _packet.Write(_radius);
            _packet.Write(_destroyAfter);
            _packet.Write(_aoeModel);
            _packet.Write(_parentId);
            _packet.Write(_parentType);

            SendTCPDataToAll(_packet);
        }
    }

    #region Projectile
    /*
        public static void SpawnProjectile(Projectile _projectile, int _thrownByPlayer)
        {
            using (Packet _packet = new Packet((int)ServerPackets.spawnProjectile))
            {
                _packet.Write(_projectile.id);
                _packet.Write(_projectile.transform.position);
                _packet.Write(_thrownByPlayer);
                SendTCPDataToAll(_packet);
            }
        }
    */
    public static void ProjectilePosition(Projectile _projectile)
    {
        using (Packet _packet = new Packet((int)ServerPackets.projectilePosition))
        {
            _packet.Write(_projectile.id);
            _packet.Write(_projectile.transform.position);
            SendUDPDataToAll(_packet);
        }
    }

    public static void ProjectileExploded(Projectile _projectile)
    {
        using (Packet _packet = new Packet((int)ServerPackets.projectileExploded))
        {
            _packet.Write(_projectile.id);
            _packet.Write(_projectile.transform.position);
            SendTCPDataToAll(_packet);
        }
    }
    #endregion

    #region Events

    public static void ResetTimer(bool _toReset)
    {
        using (Packet _packet = new Packet((int)ServerPackets.resetTimer))
        {
            _packet.Write(_toReset);

            SendTCPDataToAll(_packet);
        }
    }

    #endregion

    #region Items

    public static void CreateItemSpawner(int _toClient, int _spawnerId, Vector3 _spawnerPosition, bool _hasItem)
    {
        using (Packet _packet = new Packet((int)ServerPackets.createItemSpawner))
        {
            _packet.Write(_spawnerId);
            _packet.Write(_spawnerPosition);
            _packet.Write(_hasItem);

            SendTCPData(_toClient, _packet);
        }
    }

    public static void ItemSpawned(int _spawnerId)
    {
        using (Packet _packet = new Packet((int)ServerPackets.itemSpawned))
        {
            _packet.Write(_spawnerId);

            SendTCPDataToAll(_packet);
        }
    }

    public static void ItemPickedUp(int _spawnerId, int _byPlayer)
    {
        using (Packet _packet = new Packet((int)ServerPackets.itemPickedUp))
        {
            _packet.Write(_spawnerId);
            _packet.Write(_byPlayer);
            SendTCPDataToAll(_packet);
        }
    }

    #endregion

    #region Debug

    public static void DrawBall(Vector3 _position, float _radius, string _name)
    {
        DrawPrimitives.instance.DrawBall(_position);
        DrawPrimitives.instance.DrawVerticalRay(_position, 20.0f);

        using (Packet _packet = new Packet((int)ServerPackets.drawBall))
        {
            _packet.Write(_position);
            _packet.Write(_radius);
            _packet.Write(_name);

            SendUDPDataToAll(_packet);
        }
    }

    #endregion
}
