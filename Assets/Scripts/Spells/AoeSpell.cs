﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public abstract class AoeSpell : EffectiveSpell
{
    public AoeModel AoeModel = AoeModel.defaultModel;
    public GameObject aoe;
    public float areaRadius;
    public float destroyAfter = 1f;
    public float damageTickSpeed = 0f; //0 hits once
    public EntityType eligibleTargets = EntityType.player;
    public bool isSkillshot = false;
    public abstract bool Cast(Vector3 _target, GameObject _caster);
}
