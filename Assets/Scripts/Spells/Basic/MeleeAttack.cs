﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : EffectiveSpell
{
    public int rageGenerated = 25;
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (_target == null) return false;
        if (_caster.tag == "Player")
        {
            Player p = _caster.GetComponent<Player>();
            CombatStats pStats = p.GetComponent<CombatStats>();
            bool isRageCaster = pStats.resourceType == Constants.ResourceTypes.rage;


            float playerDamage = pStats.GetPlayerDamage();
            //p.isAttackRoutineRunning = true;

            switch (_target.tag)
            {
                case "Enemy":
                    _target.GetComponent<Enemy>().TakeDamage(playerDamage, p.id, 1f);
                    ServerSend.PlayerAttacked(p, _target.tag, _target.GetComponent<Enemy>().id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.basicAttack, Constants.VfxOnHit.noEffect);
                    break;
                case "Player":
                    if (_target.GetComponent<Player>().id == _caster.GetComponent<Player>().id) return false;
                    _target.GetComponent<Player>().TakeDamage(playerDamage);
                    ServerSend.PlayerAttacked(p, _target.tag, _target.GetComponent<Player>().id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.basicAttack, Constants.VfxOnHit.noEffect);
                    break;
            }

            if (isRageCaster)
            {
                pStats.AddRage(rageGenerated);
                ServerSend.PlayerRage(p);
            }
        }
        else if (_caster.tag == "Enemy")
        {
            if (_target.TryGetComponent(out Player p))
            {
                p.TakeDamage(_caster.GetComponent<Enemy>().damage);
            }
        }

        return true;
    }


}
