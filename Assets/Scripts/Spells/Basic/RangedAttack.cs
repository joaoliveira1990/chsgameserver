﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttack : ProjectileSpell
{
    public GameObject projectile;
    //private int projectileId = 1;
    public int damage = 0;
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            Vector3 shootOrigin = _caster.transform.GetChild(1).transform.position;
            Player p = _caster.GetComponent<Player>();
            GameObject projectileObject = Instantiate(projectile, shootOrigin, projectile.transform.rotation);
            damage = (int) p.GetComponent<CombatStats>().GetPlayerDamage();
            projectileObject.GetComponent<RangedProjectile>().Initialize(_target, damage, p.id, 0.8f);
            ServerSend.SpawnProjectile(p.shootOrigin.transform.position, _target, projectileModel);

            // Anim
            if (_target.TryGetComponent(out Enemy e))
            {
                ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.noEffect);
            }
            else if (_target.TryGetComponent(out Player targetPlayer))
            {
                ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.noEffect);
            }
        }
        else if (_caster.tag == "Enemy")
        {
            GameObject projectileObject = Instantiate(projectile, _caster.transform.position, projectile.transform.rotation);
            projectileObject.GetComponent<RangedProjectile>().Initialize(_target, 25.0f);
            Enemy e = _caster.GetComponent<Enemy>();
            ServerSend.SpawnProjectile(e.shootOrigin.transform.position, _target, this.projectileModel);
        }

        return true;
    }

    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        return true;
    }
    public override void SpellHit(Enemy _target, int playerId, float threatModifier)
    {
        _target.TakeDamage(this.damage, playerId, threatModifier); //playerid and threat for aggro calculations
        if (this.effects[0] != null)
        {
            _target.GetComponent<CombatStatsEnemy>().AddEffect(this.effects[0]);
        }
        this.timesItHit++;
    }
    public override void SpellHit(Player _target)
    {
        _target.TakeDamage(this.damage); //playerid and threat for aggro calculations
        if (this.effects[0] != null)
        {
            _target.GetComponent<CombatStats>().AddEffect(this.effects[0]);
        }
        this.timesItHit++;
    }
}
