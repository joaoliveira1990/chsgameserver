﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    [SerializeField] public Constants.EffectTypes type;
    [SerializeField] public Constants.AllEffects name;

    [Header("Stats increase")]
    [SerializeField] public bool canApplyStatChanges = false;
    [SerializeField] public float damageModifier = 0f;
    [SerializeField] public int strengthModifier = 0;
    [SerializeField] public int intellectModifier = 0;
    [SerializeField] public int agilityModifier = 0;
    [SerializeField] public int staminaModifier = 0;
    [SerializeField] public int armorModifier = 0;
    [SerializeField] public int magicResistModifier = 0;
    [SerializeField] public int healthRegenValueModifier = 0;
    [SerializeField] public int manaRegenValueModifier = 0;
    [SerializeField] public float movementSpeedModifier = 0f;
    [SerializeField] public float attackSpeedModifier = 0f;
    [SerializeField] public float critChanceModifier = 0f;
    [SerializeField] public float healingTaken = 1f;
    
    [Header("Death Immunity")]
    [SerializeField] public float healPercentage = 0.0f;
    [SerializeField] public bool deathImmunity = false;

    [Header("CC")]
    [SerializeField] public Constants.CC ccType;
    [SerializeField] public float typeDuration;

    [Header("Overtime settings")]
    [SerializeField] public bool isHeal = false;
    [SerializeField] public int damageOverTime; // TODO: This should be damagePerTick
    [SerializeField] public float damageTickSpeed; // TODO: This should be tickInterval
    [SerializeField] public int icon;
    [SerializeField] public float lifeTime;
    [SerializeField] public int stacks = 0;
    [SerializeField] public int maxStacks = 1;
    [SerializeField] public bool isDestroyed = false;
    private CombatStats targetPlayerStats = null;
    private CombatStatsEnemy targetEnemyStats = null;
    private int targetId = 0;
    public void Initialize(GameObject _target)
    {
        StartCoroutine(DestroyAfter(this.lifeTime));
        if (this.type == Constants.EffectTypes.overtime && this.damageTickSpeed > 0)
        {
            StartCoroutine(StartTickingDamage(_target));
        }

        if (_target.TryGetComponent(out Enemy e))
        {
            targetId = e.id;
            targetEnemyStats = e.GetComponent<CombatStatsEnemy>();
            if (canApplyStatChanges) ApplyStatChanges(targetEnemyStats);

            if (this.ccType != Constants.CC.none)
            {
                ApplyCC(targetEnemyStats);
            }
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            targetId = targetPlayer.id;
            targetPlayerStats = targetPlayer.GetComponent<CombatStats>();
            if (canApplyStatChanges) ApplyStatChanges(targetPlayerStats);

            if (this.ccType != Constants.CC.none)
            {
                ApplyCC(targetPlayerStats);
            }
        }
    }
    public void ApplyStatChanges(CombatStats _target)
    {
        _target.damage += damageModifier;
        _target.spellDamage += damageModifier;
        _target.strength += strengthModifier;
        _target.intellect += intellectModifier;
        _target.agility += agilityModifier;
        _target.stamina += staminaModifier;
        _target.armor += armorModifier;
        _target.magicResist += magicResistModifier;
        _target.healthRegenValue += healthRegenValueModifier;
        _target.manaRegenValue += manaRegenValueModifier;
        _target.attackSpeed += attackSpeedModifier;
        _target.movementSpeed += movementSpeedModifier;
        _target.critChance += critChanceModifier;
        _target.healingTaken += healingTaken;
        _target.healPercentage = healPercentage;
        _target.hasDeathImmunity = deathImmunity;

        ServerSend.UpdateStatWindow(targetId, _target.strength, _target.intellect, _target.agility, _target.stamina, _target.armor, _target.magicResist, _target.healthRegenValue, _target.manaRegenValue, _target.critChance, _target.attackSpeed, _target.damage, _target.spellDamage);
    }
    public void RemoveStatChanges(CombatStats _target)
    {
        _target.damage -= damageModifier;
        _target.strength -= strengthModifier;
        _target.intellect -= intellectModifier;
        _target.agility -= agilityModifier;
        _target.stamina -= staminaModifier;
        _target.armor -= armorModifier;
        _target.magicResist -= magicResistModifier;
        _target.healthRegenValue -= healthRegenValueModifier;
        _target.manaRegenValue -= manaRegenValueModifier;
        _target.attackSpeed -= attackSpeedModifier;
        _target.movementSpeed -= movementSpeedModifier;
        _target.critChance -= critChanceModifier;
        _target.healingTaken += healingTaken;
        _target.healPercentage = 0f;
        _target.hasDeathImmunity = !deathImmunity;

        ServerSend.UpdateStatWindow(targetId, _target.strength, _target.intellect, _target.agility, _target.stamina, _target.armor, _target.magicResist, _target.healthRegenValue, _target.manaRegenValue, _target.critChance, _target.attackSpeed, _target.damage, _target.spellDamage);
    }

    public void ApplyStatChanges(CombatStatsEnemy _target)
    {
        _target.movementSpeed += movementSpeedModifier;    }
    public void RemoveStatChanges(CombatStatsEnemy _target)
    {
        _target.movementSpeed -= movementSpeedModifier;
    }
    public void ApplyCC(CombatStats _target)
    {
        _target.SetCC(ccType, true, movementSpeedModifier);
    }
    public void ApplyCC(CombatStatsEnemy _target)
    {
        _target.SetCC(ccType, true, movementSpeedModifier);
    }
    public void RemoveCC(CombatStats _target)
    {
        _target.SetCC(ccType, false, -movementSpeedModifier);
    }
    public void RemoveCC(CombatStatsEnemy _target)
    {
        _target.SetCC(ccType, false, -movementSpeedModifier);
    }

    private IEnumerator StartTickingDamage(GameObject _target)
    {
        yield return new WaitForSeconds(this.damageTickSpeed);

        if (_target.TryGetComponent(out Enemy e))
        {
            // ! Fix "UpdateHealth" for Enemy
            e.TakeDamage(this.damageOverTime, _target.GetComponent<Enemy>().id, 0.8f);
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            //targetPlayer.UpdateHealth(this.damageOverTime); // TODO: value over time
            targetPlayer.TakeDamage(this.damageOverTime);
        }

        StartCoroutine(StartTickingDamage(_target));
    }
    private IEnumerator DestroyAfter(float _lifetime)
    {
        yield return new WaitForSeconds(_lifetime);

        if (targetPlayerStats != null)
        {
            if (canApplyStatChanges) RemoveStatChanges(targetPlayerStats);
            if (this.ccType != Constants.CC.none)
            {
                RemoveCC(targetPlayerStats);
            }
        }

        if (targetEnemyStats != null)
        {
            if (canApplyStatChanges) RemoveStatChanges(targetEnemyStats);
            if (this.ccType != Constants.CC.none)
            {
                RemoveCC(targetEnemyStats);
            }
        }

        this.isDestroyed = true;
        Destroy(this.gameObject);
    }

    public void RestartCoroutine()
    {
        StopAllCoroutines();
        StartCoroutine(DestroyAfter(this.lifeTime));
    }
}
