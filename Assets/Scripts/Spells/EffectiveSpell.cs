﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EffectiveSpell : Spell
{
    public int damage;
    public int heal;
    public float critChance;

    /// <summary name="effects">Attach effect prefab in editor</summary>
    public Effect[] effects;
}

// TODO: Would be useful to have method here to get effects