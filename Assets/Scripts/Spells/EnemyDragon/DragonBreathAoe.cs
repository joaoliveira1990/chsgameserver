﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonBreathAoe : AoeSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (_caster.tag == "Enemy")
        {
            isCasting = true;
            StopAllCoroutines();
            StartCoroutine(SpellCasted(_target, _caster));
            return true;
        }
        return false;
    }

    private IEnumerator SpellCasted(GameObject _target, GameObject _caster)
    {
        yield return new WaitForSeconds(castTime);

        GameObject coneObject = Instantiate(aoe, _caster.GetComponent<Enemy>().breathOrigin);
        coneObject.GetComponent<Cone>().Initialize(-1f, damage, destroyAfter, damageTickSpeed, eligibleTargets);
        ServerSend.SpawnAoe(_target.transform.position, -1, destroyAfter, (int)AoeModel, _caster.GetComponent<Enemy>().id, "enemy");
    }
    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            CastInstant(_target, _caster);
        }
        return false;
    }

    private void CastInstant(Vector3 _targetPosition, GameObject _caster)
    {
        GameObject coneObject = Instantiate(aoe, _caster.GetComponent<Player>().shootOrigin);
        coneObject.GetComponent<Cone>().Initialize(-1f, damage, destroyAfter, damageTickSpeed, eligibleTargets);
        ServerSend.SpawnAoe(_targetPosition, -1, destroyAfter, (int)AoeModel, _caster.GetComponent<Player>().id, "player");
    }
}
