﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : ProjectileSpell
{
    public GameObject projectile;
    public int damage = 200;

    //cast logic
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            if (!IsElligibleTarget(_target, _caster)) return false;
            ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, 1f, 0);
            isCasting = true;
            StopAllCoroutines();
            StartCoroutine(SpellCasted(_target, _caster));
        }
        return true;
    }


    private IEnumerator SpellCasted(GameObject _target, GameObject _caster)
    {
        yield return new WaitForSeconds(castTime);
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Vector3 shootOrigin = _caster.transform.GetChild(1).transform.position;
        GameObject projectileObject = Instantiate(projectile, shootOrigin, projectile.transform.rotation);
        float calculatedDamage = damage + casterStats.GetPlayerDamage();
        bool isCrit = Random.value < (casterStats.critChance / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;
        projectileObject.GetComponent<RangedProjectile>().Initialize(_target, calculatedDamage, p.id, 0.7f);
        ServerSend.SpawnProjectile(p.shootOrigin.transform.position, _target, Constants.ProjectileModel.fireball);

        // Anim
        if (_target.TryGetComponent(out Enemy e))
        {
            ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.fireballMuzzle);
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.fireballMuzzle);
        }

        casterStats.AddMana(-cost);
    }//cast logic

    //skillshot
    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, 1f, 0);
            isCasting = true;
            if (castTime > 0) //skillshot with cast
            {
                StopAllCoroutines();
                StartCoroutine(SpellCasted(_target, _caster));
            }
            else
            {
                CastInstant(_target, _caster);
            }
        }
        return true;
    }

    private void CastInstant(Vector3 _target, GameObject _caster)
    {
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Vector3 shootOrigin = _caster.transform.GetChild(1).transform.position;
        GameObject projectileObject = Instantiate(projectile, shootOrigin, projectile.transform.rotation);
        float calculatedDamage = damage + casterStats.GetPlayerDamage();
        bool isCrit = Random.value < (casterStats.critChance / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;
        Vector3 calculatedTarget = projectileObject.GetComponent<RangedProjectile>().Initialize(this, _caster.transform.position, _target, calculatedDamage, p.id, 0.7f, this.range, p.team, null);
        ServerSend.SpawnProjectile(p.shootOrigin.transform.position, null, Constants.ProjectileModel.fireball, calculatedTarget, this.range, p.team);
        ServerSend.PlayerAttacked(p, "Ground", -1, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.fireballMuzzle);

        casterStats.AddMana(-cost);
    }

    //casted skillshot
    private IEnumerator SpellCasted(Vector3 _target, GameObject _caster)
    {
        yield return new WaitForSeconds(castTime);
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Vector3 shootOrigin = _caster.transform.GetChild(1).transform.position;
        GameObject projectileObject = Instantiate(projectile, shootOrigin, projectile.transform.rotation);
        float calculatedDamage = damage + casterStats.GetPlayerDamage();
        bool isCrit = Random.value < (casterStats.critChance / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;
        Vector3 calculatedTarget = projectileObject.GetComponent<RangedProjectile>().Initialize(this, _caster.transform.position, _target, calculatedDamage, p.id, 0.7f, this.range, p.team, null);
        ServerSend.SpawnProjectile(p.shootOrigin.transform.position, null, Constants.ProjectileModel.fireball, calculatedTarget, this.range, p.team);
        ServerSend.PlayerAttacked(p, "Ground", -1, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.fireballMuzzle);

        casterStats.AddMana(-cost);
    }//skillshot
    public override void SpellHit(Enemy _target, int playerId, float threatModifier)
    {
        _target.TakeDamage(this.damage, playerId, threatModifier); //playerid and threat for aggro calculations
        if (this.effects.Length > 0 && this.effects[0] != null)
        {
            _target.GetComponent<CombatStatsEnemy>().AddEffect(this.effects[0]);
        }
        this.timesItHit++;
    }
    public override void SpellHit(Player _target)
    {
        _target.TakeDamage(this.damage); //playerid and threat for aggro calculations
        if (this.effects.Length > 0 && this.effects[0] != null)
        {
            _target.GetComponent<CombatStats>().AddEffect(this.effects[0]);
        }
        this.timesItHit++;
    }
}
