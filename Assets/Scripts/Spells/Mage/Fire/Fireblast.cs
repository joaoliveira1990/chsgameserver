﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireblast : EffectiveSpell
{
    //bool isOnCooldown = false;

    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsElligibleTarget(_target, _caster)) return false;
        if (isOnCooldown) return false;
        ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, cooldown, 1);
        Player p = _caster.GetComponent<Player>();
        Vector3 _targetToCaster = _target.transform.position - _caster.transform.position;
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        float calculatedDamage = damage + casterStats.intellect * 2;
        bool isCrit = Random.value < (casterStats.critChance / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;

        //add effects
        if (_target.TryGetComponent(out Enemy e))
        {
            e.TakeDamage(calculatedDamage, p.id, 0.7f);
            ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.fireblast, Constants.VfxOnHit.fireblastMuzzle);
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            targetPlayer.TakeDamage(calculatedDamage);
            ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.fireblast, Constants.VfxOnHit.fireblastMuzzle);
        }
        else return false;

        casterStats.AddMana(-cost);
        StartCoroutine(StartCooldown(cooldown));
        return true;
    }

    private IEnumerator StartCooldown(float _cooldown)
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        isOnCooldown = false;
    }
}
