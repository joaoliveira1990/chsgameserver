﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pyroblast : ProjectileSpell
{
    public GameObject projectile;
    private int projectileId = 3;
    public int damage = 300;
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            if (!IsElligibleTarget(_target, _caster)) return false;
            ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, 1f, 2);
            isCasting = true;
            StopAllCoroutines();
            StartCoroutine(SpellCasted(_target, _caster));
        }
        return true;
    }

    private IEnumerator SpellCasted(GameObject _target, GameObject _caster)
    {
        yield return new WaitForSeconds(castTime);
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Vector3 shootOrigin = _caster.transform.GetChild(1).transform.position;
        GameObject projectileObject = Instantiate(projectile, shootOrigin, projectile.transform.rotation);
        float calculatedDamage = damage + casterStats.GetPlayerDamage();
        bool isCrit = Random.value < (casterStats.critChance / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;
        projectileObject.GetComponent<RangedProjectile>().Initialize(_target, calculatedDamage, p.id, 0.7f);
        ServerSend.SpawnProjectile(p.shootOrigin.transform.position, _target, Constants.ProjectileModel.biggerFireball);

        // Anim
        if (_target.TryGetComponent(out Enemy e))
        {
            ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.pyroblastMuzzle);
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.pyroblastMuzzle);
        }

        casterStats.AddMana(-cost);
    }

    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, 1f, 0);
            isCasting = true;
            if (castTime > 0) //skillshot with cast
            {
                StopAllCoroutines();
                StartCoroutine(SpellCasted(_target, _caster));
            }
            else
            {
                CastInstant(_target, _caster);
            }
        }
        return true;
    }

    private void CastInstant(Vector3 _target, GameObject _caster)
    {
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Vector3 shootOrigin = _caster.transform.GetChild(1).transform.position;
        GameObject projectileObject = Instantiate(projectile, shootOrigin, projectile.transform.rotation);
        float calculatedDamage = damage + casterStats.GetPlayerDamage();
        bool isCrit = Random.value < (casterStats.critChance / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;
        Vector3 calculatedTarget = projectileObject.GetComponent<RangedProjectile>().Initialize(this, _caster.transform.position, _target, calculatedDamage, p.id, 0.7f, this.range, p.team, null);
        ServerSend.SpawnProjectile(p.shootOrigin.transform.position, null, Constants.ProjectileModel.biggerFireball, calculatedTarget, this.range, p.team);
        ServerSend.PlayerAttacked(p, "Ground", -1, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.pyroblastMuzzle);

        casterStats.AddMana(-cost);
    }

    private IEnumerator SpellCasted(Vector3 _target, GameObject _caster)
    {
        yield return new WaitForSeconds(castTime);
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Vector3 shootOrigin = _caster.transform.GetChild(1).transform.position;
        GameObject projectileObject = Instantiate(projectile, shootOrigin, projectile.transform.rotation);
        float calculatedDamage = damage + casterStats.GetPlayerDamage();
        bool isCrit = Random.value < (casterStats.critChance / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;
        Vector3 calculatedTarget = projectileObject.GetComponent<RangedProjectile>().Initialize(this, _caster.transform.position, _target, calculatedDamage, p.id, 0.7f, this.range, p.team, null);
        ServerSend.SpawnProjectile(p.shootOrigin.transform.position, null, Constants.ProjectileModel.biggerFireball, calculatedTarget, this.range, p.team);
        ServerSend.PlayerAttacked(p, "Ground", -1, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.pyroblastMuzzle);

        casterStats.AddMana(-cost);
    }
    public override void SpellHit(Enemy _target, int playerId, float threatModifier)
    {
        _target.TakeDamage(this.damage, playerId, threatModifier); //playerid and threat for aggro calculations
        if (this.effects.Length > 0 && this.effects[0] != null)
        {
            _target.GetComponent<CombatStatsEnemy>().AddEffect(this.effects[0]);
        }
        this.timesItHit++;
    }
    public override void SpellHit(Player _target)
    {
        _target.TakeDamage(this.damage); //playerid and threat for aggro calculations
        if (this.effects.Length > 0 && this.effects[0] != null)
        {
            _target.GetComponent<CombatStats>().AddEffect(this.effects[0]);
        }
        this.timesItHit++;
    }
}
