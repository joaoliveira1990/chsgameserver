﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MovementSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        return false;
    }

    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            CastInstant(_target, _caster);
        }
        return false;
    }

    private void CastInstant(Vector3 _targetPosition, GameObject _caster)
    {
        var agent = _caster.GetComponent<Player>().GetAgent();
        agent.Warp(_targetPosition);
        ServerSend.PlayerWarp(_caster.GetComponent<Player>(), _targetPosition);
        /*
        GameObject aoeObject = Instantiate(aoe, _caster.transform.position, Quaternion.identity);
        aoeObject.GetComponent<Aoe>().Initialize(areaRadius, damage, destroyAfter, damageTickSpeed, eligibleTargets);
        ServerSend.SpawnAoe(_caster.transform.position, areaRadius, destroyAfter, (int)AoeModel, -1, "player");
        */
    }
}
