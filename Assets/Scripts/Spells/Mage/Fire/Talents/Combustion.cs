﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combustion : EffectiveSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (isOnCooldown) return false;
        //ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, cooldown, 1);
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Effect combustionEffect = this.effects[0];

        casterStats.AddEffect(combustionEffect);
        casterStats.AddMana(-cost);

        StartCoroutine(StartCooldown(cooldown));
        return true;
    }

    private IEnumerator StartCooldown(float _cooldown)
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        isOnCooldown = false;
    }
}
