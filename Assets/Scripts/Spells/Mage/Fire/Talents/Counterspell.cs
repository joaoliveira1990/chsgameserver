﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Counterspell : EffectiveSpell
{
    //bool isOnCooldown = false;

    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsElligibleTarget(_target, _caster)) return false;
        if (isOnCooldown) return false;
        //ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, cooldown, 1);
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Effect silenceEffect = this.effects[0];

        //add effects
        if (_target.TryGetComponent(out Enemy e))
        {
            CombatStatsEnemy targetStats = _target.GetComponent<CombatStatsEnemy>();
            targetStats.AddEffect(silenceEffect);
            e.InterruptSpells();
            ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.counterspellHit, Constants.VfxOnHit.counterspellMuzzle);
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            CombatStats targetStats = _target.GetComponent<CombatStats>();
            targetStats.AddEffect(silenceEffect);
            e.InterruptSpells();
            ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.counterspellHit, Constants.VfxOnHit.counterspellMuzzle);
        }
        else return false;

        casterStats.AddMana(-cost);
        StartCoroutine(StartCooldown(cooldown));
        return true;
    }

    private IEnumerator StartCooldown(float _cooldown)
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        isOnCooldown = false;
    }
}
