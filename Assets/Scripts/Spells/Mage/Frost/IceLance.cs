﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceLance : ProjectileSpell
{
    public GameObject projectile;
    public int damage = 200;

    //cast logic
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        
        return true;
    }

    //skillshot
    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, 1f, 0);
            CastInstant(_target, _caster);
        }
        return true;
    }

    private void CastInstant(Vector3 _target, GameObject _caster)
    {
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Vector3 shootOrigin = _caster.transform.GetChild(1).transform.position;
        GameObject projectileObject = Instantiate(projectile, shootOrigin, projectile.transform.rotation);
        float calculatedDamage = damage + casterStats.GetPlayerDamage();
        bool isCrit = Random.value < (casterStats.critChance / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;

        Vector3 calculatedTarget = projectileObject.GetComponent<RangedProjectile>().Initialize(this, _caster.transform.position, _target, calculatedDamage, p.id, 0.7f, this.range, p.team, null);

        ServerSend.SpawnProjectile(p.shootOrigin.transform.position, null, Constants.ProjectileModel.iceLance, calculatedTarget, this.range, p.team);
        ServerSend.PlayerAttacked(p, "Ground", -1, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.frostMuzzle);

        casterStats.AddMana(-cost);
    }

    public override void SpellHit(Enemy _target, int playerId, float threatModifier)
    {
        int damageIncrease = 0;
        
        if(_target.GetComponent<CombatStatsEnemy>().HasEffect(Constants.AllEffects.slow))
        {
            damageIncrease = 200;
            
        }
        _target.TakeDamage(this.damage + damageIncrease, playerId, threatModifier); //playerid and threat for aggro calculations
        if (this.effects.Length > 0 && this.effects[0] != null)
        {
            _target.GetComponent<CombatStatsEnemy>().AddEffect(this.effects[0]);
        }
        this.timesItHit++;
    }
    public override void SpellHit(Player _target)
    {
        int damageIncrease = 0;

        if(_target.GetComponent<CombatStats>().HasEffect(Constants.AllEffects.slow))
        {
            damageIncrease = 200;
        }
        _target.TakeDamage(this.damage + damageIncrease); //playerid and threat for aggro calculations
        if (this.effects.Length > 0 && this.effects[0] != null)
        {
            _target.GetComponent<CombatStats>().AddEffect(this.effects[0]);
        }
        this.timesItHit++;
    }
}
