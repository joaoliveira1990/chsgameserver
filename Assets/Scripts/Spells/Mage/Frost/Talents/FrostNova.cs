﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostNova : AoeSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (_caster.tag == "Enemy")
        {
            isCasting = true;
            StopAllCoroutines();
            StartCoroutine(SpellCasted(_target, _caster));
            return true;
        }
        return false;
    }

    private IEnumerator SpellCasted(GameObject _target, GameObject _caster)
    {
        yield return new WaitForSeconds(castTime);
        //aoe is instanced in the targets position
        GameObject aoeObject = Instantiate(aoe, _target.transform.position, Quaternion.identity);
        aoeObject.GetComponent<Aoe>().Initialize(areaRadius, damage, destroyAfter, damageTickSpeed, eligibleTargets, this.effects);
        ServerSend.SpawnAoe(_target.transform.position, areaRadius, destroyAfter, (int)AoeModel, -1, "enemy");
    }

    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            CastInstant(_target, _caster);
        }
        return false;
    }

    private void CastInstant(Vector3 _targetPosition, GameObject _caster)
    {
        GameObject aoeObject = Instantiate(aoe, _caster.transform.position, Quaternion.identity);
        aoeObject.GetComponent<Aoe>().Initialize(areaRadius, damage, destroyAfter, damageTickSpeed, eligibleTargets, this.effects);
        ServerSend.SpawnAoe(_caster.transform.position, areaRadius, destroyAfter, (int)AoeModel, -1, "player");
    }
}
