﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public abstract class MovementSpell : Spell
{
    //skillshot
    public abstract bool Cast(Vector3 _target, GameObject _caster);
    public bool isSkillshot = false;
}
