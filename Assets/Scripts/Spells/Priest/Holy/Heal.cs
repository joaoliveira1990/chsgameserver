﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : Spell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {

        if (!IsCastable(_target, _caster, Constants.ResourceTypes.mana)) return false;

        Player p = _caster.GetComponent<Player>();
        Player targetP = TargetSelfByDefault(p, _target);
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        CombatStats targetStats = _target.GetComponent<CombatStats>();

        // TODO: Outsource to a method in Spell
        isCasting = true;
        StopAllCoroutines();
        StartCoroutine(SpellCasted(_target, _caster));

        return true;
    }

    private IEnumerator SpellCasted(GameObject _target, GameObject _caster)
    {
        yield return new WaitForSeconds(castTime);

        Player p = _caster.GetComponent<Player>();
        Player targetPlayer = _target.GetComponent<Player>();

        CombatStats casterStats = _caster.GetComponent<CombatStats>();

        if (targetPlayer != null)
        {
            targetPlayer.HealDamage(300.0f);
            ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.noAttackAnimation, Constants.VfxOnHit.heal, Constants.VfxOnHit.healMuzzle);
        }

        ManageCooldown();
        casterStats.AddMana(-this.cost);
    }
}
