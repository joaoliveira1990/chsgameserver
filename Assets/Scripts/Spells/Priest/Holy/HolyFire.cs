using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolyFire : EffectiveSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsCastable(_target, _caster, Constants.ResourceTypes.mana)) return false;

        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Effect holyFireEffect = this.effects[0];

        ManageCooldown();

        casterStats.AddMana(-this.cost);

        // - Damage & Animation
        if (_target.TryGetComponent(out Enemy e))
        {
            CombatStatsEnemy targetStats = _target.GetComponent<CombatStatsEnemy>();
            targetStats.AddEffect(holyFireEffect);

            e.TakeDamage(this.damage, p.id, 0.8f);
            ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.holyFire, Constants.VfxOnHit.holyFireMuzzle);
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            CombatStats targetStats = _target.GetComponent<CombatStats>();
            targetStats.AddEffect(holyFireEffect);

            targetPlayer.TakeDamage(this.damage);
            ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.holyFire, Constants.VfxOnHit.holyFireMuzzle);
        }

        return true;
    }
}
