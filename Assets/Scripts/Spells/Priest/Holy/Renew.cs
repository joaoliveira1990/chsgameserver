﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Make this spell work with enemies
// TODO: Outsource all CD methods to a method in spell class
// TODO: Maybe outsource selfcast rule to a method in spell class
// TODO: Make method in spell to "Check castable and manage resources"

public class Renew : EffectiveSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsCastable(_target, _caster, Constants.ResourceTypes.mana)) return false;

        Player p = _caster.GetComponent<Player>();
        Player targetP = TargetSelfByDefault(p, _target);
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        CombatStats targetStats = _target.GetComponent<CombatStats>();

        ManageCooldown();

        Effect renewEffect = this.effects[0];
        targetStats.AddEffect(renewEffect);

        casterStats.AddMana(-this.cost);
        targetP.HealDamage(100.0f);

        ServerSend.PlayerAttacked(p, targetP.tag, targetP.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.renew, Constants.VfxOnHit.renewMuzzle);

        return true;
    }
}
