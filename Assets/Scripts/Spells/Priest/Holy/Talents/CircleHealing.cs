﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleHealing : AoeSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        return false;
    }

    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        ServerLogging.instance.Debugger($"mouse target coords = {_target}");
        
        if (_caster.tag == "Player")
        {
            CastInstant(_target, _caster);
        }
        return false;
    }

    private void CastInstant(Vector3 _targetPosition, GameObject _caster)
    {
        GameObject aoeObject = Instantiate(aoe, _targetPosition, Quaternion.identity);
        aoeObject.GetComponent<Aoe>().Initialize(areaRadius, -heal, destroyAfter, damageTickSpeed, eligibleTargets, this.effects);
        ServerSend.SpawnAoe(_targetPosition, areaRadius, destroyAfter, (int)AoeModel, -1, "player"); // parent id < 0 if aoe spawns at mouse location
    }
}
