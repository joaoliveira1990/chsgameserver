﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Empower : EffectiveSpell
{
public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsCastable(_target, _caster, Constants.ResourceTypes.mana)) return false;

        Player p = _caster.GetComponent<Player>();
        Player targetP = TargetSelfByDefault(p, _target);
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        CombatStats targetStats = _target.GetComponent<CombatStats>();

        ManageCooldown();

        Effect avatarEffect = this.effects[0];
        targetStats.AddEffect(avatarEffect);

        casterStats.AddMana(-this.cost);

        ServerSend.PlayerIncreaseSizeForDuration(targetP.id, 0.5f, avatarEffect.lifeTime);
        
        ServerSend.PlayerAttacked(p, targetP.tag, targetP.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.renew, Constants.VfxOnHit.renewMuzzle);

        return true;
    }
}
