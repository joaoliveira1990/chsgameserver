﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardianSpirit : EffectiveSpell
{
public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsCastable(_target, _caster, Constants.ResourceTypes.mana)) return false;

        Player p = _caster.GetComponent<Player>();
        Player targetP = TargetSelfByDefault(p, _target);
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        CombatStats targetStats = _target.GetComponent<CombatStats>();

        ManageCooldown();

        Effect deathImmunityEffect = this.effects[0];
        targetStats.AddEffect(deathImmunityEffect);

        casterStats.AddMana(-this.cost);
        
        ServerSend.PlayerAttacked(p, targetP.tag, targetP.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.renew, Constants.VfxOnHit.renewMuzzle);

        return true;
    }
}
