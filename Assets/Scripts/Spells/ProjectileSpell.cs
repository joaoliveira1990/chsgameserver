﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Constants;

public abstract class ProjectileSpell : Spell
{
    public ProjectileModel projectileModel = ProjectileModel.defaultModel;

    //skillshot
    public abstract bool Cast(Vector3 _target, GameObject _caster);

    public abstract void SpellHit(Enemy _target, int playerId, float threatModifier);
    public abstract void SpellHit(Player _target);
    public bool isSkillshot = false;
    [SerializeField] public Effect[] effects;
    public int timesItHit = 0;
}
