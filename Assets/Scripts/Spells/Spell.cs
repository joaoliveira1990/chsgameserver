﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spell : MonoBehaviour
{
    public SpellClientIdMap.ClientHotbarSpellsId id;
    public string spellName;
    public string description;
    public int icon;
    public int cost;
    public Constants.ResourceTypes resource;
    // public enum ResourceTypes
    // {
    //     mana,
    //     rage,
    //     energy
    // };
    public AbilityType abilityType;
    public enum AbilityType
    {
        damage,
        heal
    }
    public float castTime;
    public bool isCasting = false;
    public bool isSelfCast = false;
    public float cooldown;
    [NonSerialized] public bool isOnCooldown = false; // tODO: make this private
    public float range = 5.0f;
    public spellType type;
    public enum spellType
    {
        melee,
        fire,
        frost,
        arcane,
    };
    public string[] validTargets;
    public float delayTime;
    public string castAnimation = "";

    /// <returns>"true" if spell was casted</returns>
    public abstract bool Cast(GameObject _target, GameObject _caster);

    public void InterruptSpell()
    {
        StopAllCoroutines();
    }

    /// <returns>
    /// If ability is "damage", return "false" if is a player from another team
    /// If ability is "heal", return "false" if is an enemy or a player from another team
    ///</returns>
    public bool IsElligibleTarget(GameObject _target, GameObject _caster)
    {
        int casterTeam = _caster.GetComponent<Player>().team;

        if (abilityType == AbilityType.damage)
        {
            if (_target.tag == "Player" && _target.GetComponent<Player>().team == casterTeam) return false;
            else return true;
        }

        if (abilityType == AbilityType.heal)
        {
            if (_target.transform.TryGetComponent(out Player targetPlayer))
            {
                if (targetPlayer.team == casterTeam) return true;
            }
            return false;
        }

        return false;
    }

    public bool HasEnoughResource(Player _caster, Constants.ResourceTypes _resourceType)
    {
        // TODO: should resource agnostic
        CombatStats casterStats = _caster.GetComponent<CombatStats>();

        switch (_resourceType)
        {
            case Constants.ResourceTypes.mana:
                return this.cost <= casterStats.mana;
            case Constants.ResourceTypes.rage:
                return this.cost <= casterStats.rage;
            case Constants.ResourceTypes.energy:
                return this.cost <= casterStats.energy;
            default:
                return false;
        }
    }

    /// <summary>Routine check for: valid target, spell CD and enough resource</summary>
    public bool IsCastable(GameObject _target, GameObject _caster, Constants.ResourceTypes _resourceType)
    {
        Player p = _caster.GetComponent<Player>();

        if (!IsElligibleTarget(_target, _caster)) return false;
        if (this.isOnCooldown) return false;
        if (!HasEnoughResource(p, _resourceType)) return false;

        return true;
    }

    /// <summary>Target Self when not selecting any target Player</summary>
    public Player TargetSelfByDefault(Player _casterPlayer, GameObject _target)
    {
        Player targetP = _target.GetComponent<Player>();
        if (_target == null && this.abilityType == AbilityType.heal) return _casterPlayer;
        else return targetP;
    }


    public void ManageCooldown()
    {
        StartCoroutine(StartCooldown(this.cooldown));
        // ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, cooldown, 1);
    }

    private IEnumerator StartCooldown(float _cooldown)
    {
        this.isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        this.isOnCooldown = false;
    }
}
