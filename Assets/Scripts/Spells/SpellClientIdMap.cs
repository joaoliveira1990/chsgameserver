﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellClientIdMap : MonoBehaviour
{
    //client needs same IDS so it is possible to dragndrop spells on the client side and still cast the right one
    public enum ClientHotbarSpellsId
    {
        Fireball = 1,
        Pyroblast,
        Fireblast,
        Overpower,
        MortalStrike,
        Execute,
        Heal,
        HolyFire,
        Renew,
        DragonBreath,
        Blastwave,
        Meteor,
        Blink,
        Counterspell,
        Combustion,
        Charge,
        Whirlwind,
        Avatar,
        CircleHealing,
        GuardianSpirit,
        Empower,
        Devastate,
        ThunderClap,
        ShieldBlock,
        Taunt,
        Shockwave,
        Frostbolt,
        IceLance,
        GlacialSpike,
        ConeOfCold,
        FrostNova,
        FrozenOrb
    }
}
