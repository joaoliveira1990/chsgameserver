﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Charge : EffectiveSpell
{

    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsElligibleTarget(_target, _caster)) return false;
        if (this.isOnCooldown) return false;
        ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, cooldown, 2);
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Effect stunEffect = this.effects[0];
        float currentRage = casterStats.rage;
        NavMeshAgent agent = _caster.GetComponent<Player>().GetComponent<NavMeshAgent>();

        float currentSpeed = agent.speed;
        agent.speed *= 10;
        float newSpeed = agent.speed;

        //add effects
        if (_target.TryGetComponent(out Enemy e))
        {
            CombatStatsEnemy targetStats = _target.GetComponent<CombatStatsEnemy>();
            targetStats.AddEffect(stunEffect);
            e.InterruptSpells();
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            CombatStats targetStats = _target.GetComponent<CombatStats>();
            targetStats.AddEffect(stunEffect);
            targetPlayer.InterruptSpells();
        }
        else return false;

        p.MovePlayerAndSendPosition(_target.transform.position);

        ServerSend.PlayerSetSpeed(p.id, newSpeed); //add current time so client can stop it
        StartCoroutine(Charging(currentSpeed, p));

        StartCoroutine(StartCooldown(cooldown));
        return true;
    }

    private IEnumerator Charging(float _currentSpeed, Player _playerCaster)
    {
        yield return new WaitForSeconds(0.5f);
        ServerSend.PlayerSetSpeed(_playerCaster.id, _currentSpeed);
        _playerCaster.StopPath();
        _playerCaster.GetComponent<NavMeshAgent>().speed = _currentSpeed;
    }


    private IEnumerator StartCooldown(float _cooldown)
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        isOnCooldown = false;
    }

}
