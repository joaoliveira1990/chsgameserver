﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Execute : EffectiveSpell
{

    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsElligibleTarget(_target, _caster)) return false;
        if (this.isOnCooldown) return false;
        ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, cooldown, 2);
        Player p = _caster.GetComponent<Player>();
        Vector3 _targetToCaster = _target.transform.position - _caster.transform.position;
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        float currentRage = casterStats.rage;
        float calculatedDamage = damage + casterStats.strength * 4 + currentRage * 4;
        bool isCrit = Random.value < (critChance + casterStats.agility / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;

        //add effects
        if (_target.TryGetComponent(out Enemy e))
        {
            if (e.health * (100 / e.maxHealth) > 80f) return false;
            e.TakeDamage(calculatedDamage, p.id, 1f);
            ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.attack2, Constants.VfxOnHit.execute, Constants.VfxOnHit.executeMuzzle);
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            if (targetPlayer.health * (100 / targetPlayer.maxHealth) > 30f) return false;
            targetPlayer.TakeDamage(calculatedDamage);
            ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.attack2, Constants.VfxOnHit.execute, Constants.VfxOnHit.executeMuzzle);
        }
        else return false;

        casterStats.ResetRage();
        ServerSend.PlayerRage(_caster.GetComponent<Player>());
        StartCoroutine(StartCooldown(cooldown));
        return true;
    }

    private IEnumerator StartCooldown(float _cooldown)
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        isOnCooldown = false;
    }

}
