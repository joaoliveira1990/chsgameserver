﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortalStrike : EffectiveSpell
{
    //bool isOnCooldown = false;

    public override bool Cast(GameObject _target, GameObject _caster)
    {
        // TODO: Decide resource based on _caster argument
        if (!IsCastable(_target, _caster, Constants.ResourceTypes.rage)) return false;

        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();

        Effect overpowerEffect = casterStats.GetEffectByName(Constants.AllEffects.overpower);
        float damageModifier = 0f;
        int numberOfStacks = 0;
        if (overpowerEffect != null)
        {
            damageModifier = overpowerEffect.damageModifier;
            numberOfStacks = overpowerEffect.stacks;
        }

        bool isCrit = Random.value < (critChance + casterStats.agility / 100);
        float calculatedDamage = this.damage + casterStats.strength * 2 + casterStats.agility * 1;
        calculatedDamage = numberOfStacks > 0 ? (calculatedDamage * (1 + damageModifier)) * numberOfStacks : calculatedDamage;
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;

        if (_target.TryGetComponent(out Enemy e))
        {
            e.TakeDamage(calculatedDamage, p.id, 1.0f);
            ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.attack2, Constants.VfxOnHit.mortalStrike, Constants.VfxOnHit.mortalStrikeMuzzle);
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            targetPlayer.TakeDamage(calculatedDamage);
            ServerSend.PlayerAttacked(p, _target.tag, targetPlayer.id, PlayerClientAnimations.attack2, Constants.VfxOnHit.mortalStrike, Constants.VfxOnHit.mortalStrikeMuzzle);
        }
        else return false;

        casterStats.RemoveEffectByName(Constants.AllEffects.overpower);

        ManageCooldown();
        casterStats.AddRage(-this.cost);

        return true;
    }
}
