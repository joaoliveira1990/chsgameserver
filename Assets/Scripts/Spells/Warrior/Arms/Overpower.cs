﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Overpower : EffectiveSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsElligibleTarget(_target, _caster)) return false;
        if (this.isOnCooldown) return false;

        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();

        Vector3 _targetToCaster = _target.transform.position - _caster.transform.position;
        float calculatedDamage = damage + casterStats.strength * 2 + casterStats.agility * 1 + 500f;
        bool isCrit = Random.value < (critChance + casterStats.agility / 100);
        calculatedDamage = isCrit ? calculatedDamage * 2 : calculatedDamage;

        //add effects
        if (_target.TryGetComponent(out Enemy e))
        {
            e.TakeDamage(calculatedDamage, p.id, 1.0f);
            ServerSend.PlayerAttacked(
                p,
                _target.tag,
                e.id,
                PlayerClientAnimations.basicAttack,
                Constants.VfxOnHit.noEffect,
                Constants.VfxOnHit.overpower
            );
        }
        else if (_target.TryGetComponent(out Player targetPlayer))
        {
            targetPlayer.TakeDamage(calculatedDamage);
            ServerSend.PlayerAttacked(
                p,
                _target.tag, 
                targetPlayer.id,
                PlayerClientAnimations.basicAttack,
                Constants.VfxOnHit.noEffect,
                Constants.VfxOnHit.overpower
            );
        }
        else return false;

        //add overpower damage stacking buff to player combat stats
        casterStats.CleanEffects();

        Effect overpowerEffect = this.effects[0];

        // ! Change this to GetEffectByName and get counts
        List<Effect> stacks = casterStats.GetEffectsByName(overpowerEffect.name);

        if (stacks.Count < 3) casterStats.AddEffect(overpowerEffect);

        foreach (Effect stack in stacks)
        {
            stack.RestartCoroutine();
        }

        casterStats.AddRage(cost);
        ServerSend.PlayerRage(_caster.GetComponent<Player>());
        StartCoroutine(StartCooldown(cooldown));
        return true;
    }

    private IEnumerator StartCooldown(float _cooldown)
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        isOnCooldown = false;
    }
}
