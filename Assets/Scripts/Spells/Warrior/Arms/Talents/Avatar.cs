﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Avatar : EffectiveSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (isOnCooldown) return false;
        //ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, cooldown, 1);
        Player p = _caster.GetComponent<Player>();
        CombatStats casterStats = _caster.GetComponent<CombatStats>();
        Effect avatarEffect = this.effects[0];

        casterStats.AddEffect(avatarEffect);
        casterStats.AddRage(-cost);

        ServerSend.PlayerIncreaseSizeForDuration(p.id, 0.5f, avatarEffect.lifeTime);

        StartCoroutine(StartCooldown(cooldown));
        return true;
    }

    private IEnumerator StartCooldown(float _cooldown)
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        isOnCooldown = false;
    }
}
