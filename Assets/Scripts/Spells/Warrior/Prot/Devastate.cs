﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Devastate : AoeSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        return false;
    }

    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            CastInstant(_target, _caster);
        }
        return false;
    }

    private void CastInstant(Vector3 _targetPosition, GameObject _caster)
    {
        GameObject coneObject = Instantiate(aoe, _caster.GetComponent<Player>().shootOrigin);
        coneObject.GetComponent<Cone>().Initialize(-1f, damage, destroyAfter, damageTickSpeed, eligibleTargets);
        ServerSend.SpawnAoe(_targetPosition, -1, destroyAfter, (int)AoeModel, _caster.GetComponent<Player>().id, "player");

        ServerSend.PlayerAttacked(_caster.GetComponent<Player>(), "none", -1, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.fireballMuzzle);
    }
}
