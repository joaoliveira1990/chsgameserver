﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Taunt : EffectiveSpell
{
    //bool isOnCooldown = false;

    public override bool Cast(GameObject _target, GameObject _caster)
    {
        if (!IsElligibleTarget(_target, _caster)) return false;
        if (isOnCooldown) return false;
        ServerSend.ShowAbilityCooldown(_caster.GetComponent<Player>().id, cooldown, 1);
        Player p = _caster.GetComponent<Player>();
        Vector3 _targetToCaster = _target.transform.position - _caster.transform.position;
        CombatStats casterStats = _caster.GetComponent<CombatStats>();

        //add effects
        if (_target.TryGetComponent(out Enemy e))
        {
            Dictionary<int, float> aggroTable = e.GetAggroTable();
            List<KeyValuePair<int, float>> orderedAggroTable = aggroTable.OrderBy(d => -d.Value).ToList();

            int playerIdWithMostThreat = orderedAggroTable[0].Key;
            float maxThreatOnTarget = orderedAggroTable[0].Value;
            float casterThreat = e.GetThreadByPlayerId(p.id);

            e.SetAggroTable(playerIdWithMostThreat, casterThreat);
            e.SetAggroTable(p.id, maxThreatOnTarget + 100f);
            e.SetTargetWithMostThreat();

            ServerSend.PlayerAttacked(p, _target.tag, e.id, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.fireblast, Constants.VfxOnHit.fireblastMuzzle);
        }
        else return false;

        casterStats.AddMana(-cost);
        StartCoroutine(StartCooldown(cooldown));
        return true;
    }

    private IEnumerator StartCooldown(float _cooldown)
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(_cooldown);
        isOnCooldown = false;
    }
}
