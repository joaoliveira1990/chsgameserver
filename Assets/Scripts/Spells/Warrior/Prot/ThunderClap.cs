﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderClap : AoeSpell
{
    public override bool Cast(GameObject _target, GameObject _caster)
    {
        return false;
    }

    public override bool Cast(Vector3 _target, GameObject _caster)
    {
        if (_caster.tag == "Player")
        {
            CastInstant(_target, _caster);
        }
        return false;
    }

    private void CastInstant(Vector3 _targetPosition, GameObject _caster)
    {
        GameObject aoeObject = Instantiate(aoe, _caster.transform.position, Quaternion.identity);
        aoeObject.GetComponent<Aoe>().Initialize(areaRadius, damage, destroyAfter, damageTickSpeed, eligibleTargets, this.effects);
        ServerSend.SpawnAoe(_caster.transform.position, areaRadius, destroyAfter, (int)AoeModel, -1, "player");
        
        ServerSend.PlayerAttacked(_caster.GetComponent<Player>(), "none", -1, PlayerClientAnimations.basicAttack, Constants.VfxOnHit.noEffect, Constants.VfxOnHit.fireballMuzzle);
    }
}
