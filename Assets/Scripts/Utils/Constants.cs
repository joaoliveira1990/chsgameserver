﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const int TICKS_PER_SEC = 30;
    public const int MS_PER_TICK = 1000 / TICKS_PER_SEC;

    public enum ResourceTypes
    {
        rage,
        mana,
        energy
    }

    public enum EntityType
    {
        player,
        enemy
    }

    public enum ProjectileModel // is in the same order as the array of projectiles in the clients
    {
        defaultModel,
        arrow,
        mageBasic,
        fireball,
        biggerFireball,
        priestBasic,
        frostbolt,
        iceLance,
        glacialSpike
    }

    public enum AoeModel // is in the same order as the array of projectiles in the clients
    {
        defaultModel,
        AoeSpell1,
        DragonBreath,
        LavaBomb,
        BlastWave,
        Meteor,
        Whirlwind,
        CircleHealing,
        Devastate,
        ThunderClap,
        Shockwave,
        ConeOfCold,
        FrostNova,
        FrozenOrb
    }

    public enum CC
    {
        stun,
        snare,
        silence,
        slow,
        none
    }

    public enum EnemyTypes // is in the same order as the array of projectiles in the clients
    {
        meleeNormal,
        meleeElite,
        meleeBoss,
        rangedNormal,
        rangedElite,
        rangedBoss,
        dragonBoss,
        casterElite,
        casterBoss
    }

    public enum EffectTypes
    {
        overtime,
        passive
    }

    public enum Classes
    {
        warrior = 1,
        mage,
        priest,
        hunter,
        paladin,
        deathknight,
        warlock,
        druid,
        rogue,
        shaman
    }

    public enum WarriorSpecs
    {
        fury = 1,
        arms,
        protection
    }

    public enum MageSpecs
    {
        fire = 1,
        frost,
        arcane
    }

    public enum PriestSpecs
    {
        holy = 1,
        discipline,
        shadow
    }

    public enum VfxOnHit
    {
        noEffect = -1,
        // Basic Attacks
        basicAttack = 0,
        // Warrior
        execute,
        executeMuzzle,
        mortalStrike,
        mortalStrikeMuzzle,
        overpower,
        // Mage
        fireblast,
        fireblastMuzzle,
        fireballMuzzle,
        pyroblastMuzzle,
        // Priest
        heal,
        healMuzzle,
        holyFire,
        holyFireMuzzle,
        renew,
        renewMuzzle,
        counterspellHit,
        counterspellMuzzle,
        frostMuzzle,
    }

    public enum AllEffects
    {
        overpower = 0,
        holyFire,
        renew,
        silence,
        combustion,
        stun,
        avatar,
        deathImmunity,
        damageReduction,
        slow,
        snare
    }

    // TODO: Use these instead of strings
    public enum MouseModes
    {
        none,
        attack,
        support
    }
}
