﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataResources : MonoBehaviour
{
    static public void SetPlayerStats(Player player)
    {
        CombatStats cStats = player.GetComponent<CombatStats>();

        switch (player.selectedClass)
        {
            case (int)Constants.Classes.warrior:
                player.SetHealth(20000);
                cStats.critChance += 2 + cStats.agility / 100;
                cStats.attackSpeed = 1f;
                cStats.resourceType = Constants.ResourceTypes.rage;
                cStats.rage = 0f;
                cStats.strength = 40;
                cStats.damage = cStats.strength * 2f;
                break;
            case (int)Constants.Classes.mage:
                player.SetHealth(20000);
                cStats.critChance += 1 + cStats.intellect / 100;
                cStats.attackSpeed = 2f;
                cStats.resourceType = Constants.ResourceTypes.mana;
                cStats.maxMana = 2000f;
                cStats.mana = cStats.maxMana;
                cStats.intellect = 40;
                cStats.damage = cStats.intellect * 1.5f;
                cStats.manaRegenValue = (int)(cStats.maxMana * 0.1);
                break;
            case (int)Constants.Classes.priest:
                player.SetHealth(4000);
                cStats.critChance += 1 + cStats.intellect / 100;
                cStats.attackSpeed = 1.5f;
                cStats.resourceType = Constants.ResourceTypes.mana;
                cStats.maxMana = 2000f;
                cStats.mana = cStats.maxMana;
                cStats.intellect = 40;
                cStats.damage = cStats.intellect * 1.5f;
                cStats.manaRegenValue = (int)(cStats.maxMana * 0.1);
                break;
            default:
                break;
        }
    }

}
