﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerLogging : MonoBehaviour
{
    [SerializeField] bool isOn = true;
    public static ServerLogging instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    public void Debugger(string _message)
    {
        if (this.isOn)
        {
            Debug.Log($"-> {_message}");
        }
    }
}
