# Installation
 
This project uses .NET Framework 4.7.1 Developer Pack - [Microsoft Download](https://dotnet.microsoft.com/download/dotnet-framework/net471).
 
# Usage


# Development
 
Standard IDE for this project is VSCode, but any other IDE suitable for C# projects are also viable.

## VSCode Configuration
Settings.json file
 
```
{
    "tabnine.experimentalAutoImports": true,
    "files.exclude": {
        "**/*.meta": true
      },
}
```

## Notation

Regarding C# scripts, there are some convention we try to follow:

- Variable names
  - class attribute with `this` (example: `this.someVar`)
  - function/method parameter with `_` before (example: `_someVar`)
  - local variable with no changes to its name

## Adding Content

### Spells and effects

Effects are connected between the server and the client. `Constants.AllEffects` exist in both. In the client, also:

- add to "AllEffects" enum name of effect
- add an icon effect prefab (w/ EffectIcon script and correct effect name) to the HUBUpdater script in the Camera prefab (try to maintain enum order)

Spells are enumerated in `Constants` for each class. A direct correspondence must exist with the GameManager VfxManager arrays.

